﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





extern Il2CppGenericClass* const g_Il2CppGenericTypes[];
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[];
extern const Il2CppGenericMethodFunctionsDefinitions g_Il2CppGenericMethodFunctions[];
extern const Il2CppType* const  g_Il2CppTypeTable[];
extern const Il2CppMethodSpec g_Il2CppMethodSpecTable[];
IL2CPP_EXTERN_C_CONST int32_t* g_FieldOffsetTable[];
IL2CPP_EXTERN_C_CONST Il2CppTypeDefinitionSizes* g_Il2CppTypeDefinitionSizesTable[];
extern void** const g_MetadataUsages[];
IL2CPP_EXTERN_C const Il2CppMetadataRegistration g_MetadataRegistration;
const Il2CppMetadataRegistration g_MetadataRegistration = 
{
	7952,
	g_Il2CppGenericTypes,
	4929,
	g_Il2CppGenericInstTable,
	47467,
	g_Il2CppGenericMethodFunctions,
	27202,
	g_Il2CppTypeTable,
	58247,
	g_Il2CppMethodSpecTable,
	8926,
	g_FieldOffsetTable,
	8926,
	g_Il2CppTypeDefinitionSizesTable,
	126651,
	g_MetadataUsages,
};
