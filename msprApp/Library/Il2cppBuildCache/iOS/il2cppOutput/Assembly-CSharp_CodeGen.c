﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 <id>j__TPar <>f__AnonymousType0`4::get_id()
// 0x00000002 <seq>j__TPar <>f__AnonymousType0`4::get_seq()
// 0x00000003 <name>j__TPar <>f__AnonymousType0`4::get_name()
// 0x00000004 <data>j__TPar <>f__AnonymousType0`4::get_data()
// 0x00000005 System.Void <>f__AnonymousType0`4::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
// 0x00000006 System.Boolean <>f__AnonymousType0`4::Equals(System.Object)
// 0x00000007 System.Int32 <>f__AnonymousType0`4::GetHashCode()
// 0x00000008 System.String <>f__AnonymousType0`4::ToString()
// 0x00000009 UnityEngine.GameObject AnchorCreator::get_AnchorPrefab()
extern void AnchorCreator_get_AnchorPrefab_mBBAF7805E9D9F8D79408EE642D153BC76306B25A (void);
// 0x0000000A System.Void AnchorCreator::set_AnchorPrefab(UnityEngine.GameObject)
extern void AnchorCreator_set_AnchorPrefab_m53CC9CC3022C713826B44536B48B462C8A1FACFF (void);
// 0x0000000B System.Void AnchorCreator::RemoveAllAnchors()
extern void AnchorCreator_RemoveAllAnchors_m6BA302DD9EECA47969FD3F6E5397441DCB09ED21 (void);
// 0x0000000C System.Void AnchorCreator::Awake()
extern void AnchorCreator_Awake_m8820A3F157354D7EDED7B2D7CE2BFA42844F1F8E (void);
// 0x0000000D System.Void AnchorCreator::Update()
extern void AnchorCreator_Update_mA0A8BCCBAB0AE50DB087524E6273D11F1D6456D0 (void);
// 0x0000000E System.Void AnchorCreator::.ctor()
extern void AnchorCreator__ctor_m702B01425680D7FD1C9272A423AAF8A913E5476E (void);
// 0x0000000F System.Void AnchorCreator::.cctor()
extern void AnchorCreator__cctor_mF5AE519F086E52A01EA091115062D84D9F094F34 (void);
// 0x00000010 System.Single ARFeatheredPlaneMeshVisualizer::get_featheringWidth()
extern void ARFeatheredPlaneMeshVisualizer_get_featheringWidth_m14D3A8BE3E9A745E6FD525B19ADDC892B8399B4D (void);
// 0x00000011 System.Void ARFeatheredPlaneMeshVisualizer::set_featheringWidth(System.Single)
extern void ARFeatheredPlaneMeshVisualizer_set_featheringWidth_mD616A09A3B426EA5DE1FA37334DD194E43EEC110 (void);
// 0x00000012 System.Void ARFeatheredPlaneMeshVisualizer::Awake()
extern void ARFeatheredPlaneMeshVisualizer_Awake_mC5DB0414A2514BF4851266C25141C903F0AC57BA (void);
// 0x00000013 System.Void ARFeatheredPlaneMeshVisualizer::OnEnable()
extern void ARFeatheredPlaneMeshVisualizer_OnEnable_m8781C85CFED871C8A81A5B88DB1031856E0FC9F0 (void);
// 0x00000014 System.Void ARFeatheredPlaneMeshVisualizer::OnDisable()
extern void ARFeatheredPlaneMeshVisualizer_OnDisable_m2343B05B1A8F14BAD4DD516C584281B66FE6A4E8 (void);
// 0x00000015 System.Void ARFeatheredPlaneMeshVisualizer::ARPlane_boundaryUpdated(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_mB3D9BBD14EA1FE3ECDBACC2DB89C1B110B8B6B5F (void);
// 0x00000016 System.Void ARFeatheredPlaneMeshVisualizer::GenerateBoundaryUVs(UnityEngine.Mesh)
extern void ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_mF756D3C1F7925A69CD8C7C8CCE56209AB321FEF5 (void);
// 0x00000017 System.Void ARFeatheredPlaneMeshVisualizer::.ctor()
extern void ARFeatheredPlaneMeshVisualizer__ctor_m9A77651BCAE58AA0B994FFF6C6B63B1CFF2729F6 (void);
// 0x00000018 System.Void ARFeatheredPlaneMeshVisualizer::.cctor()
extern void ARFeatheredPlaneMeshVisualizer__cctor_mF4BA6DDB611A3FD966C8B9494AE6B3EB8647CEAD (void);
// 0x00000019 System.Void GameManager::Start()
extern void GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2 (void);
// 0x0000001A System.Void GameManager::Update()
extern void GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41 (void);
// 0x0000001B System.Void GameManager::HandleWebFnCall(System.String)
extern void GameManager_HandleWebFnCall_m376ED43FA51805EF23D17DD050E5B73897EC6185 (void);
// 0x0000001C System.Void GameManager::.ctor()
extern void GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368 (void);
// 0x0000001D System.Void Rotate::Start()
extern void Rotate_Start_mD322E77A3CF2BEF28C4DF71D3F529107F511B1FB (void);
// 0x0000001E System.Void Rotate::Update()
extern void Rotate_Update_m73D585515036D9B7AAD8336BFB8567283CE4C7E7 (void);
// 0x0000001F System.Void Rotate::SetRotationSpeed(System.String)
extern void Rotate_SetRotationSpeed_mD754DB628A79A7B2ADAED25DE732A29E7548F2F1 (void);
// 0x00000020 System.Void Rotate::.ctor()
extern void Rotate__ctor_m0EE5CC8EB699542BFC438DC3D547D39E442E9EE4 (void);
// 0x00000021 System.Void SceneLoader::Start()
extern void SceneLoader_Start_mB9AA9E8ADCE59F893E3EF8E891ED5E1F3AB80DA0 (void);
// 0x00000022 System.Void SceneLoader::Update()
extern void SceneLoader_Update_m1F381D68B1B1F69A0F9A5876813BB7D437E3A713 (void);
// 0x00000023 System.Void SceneLoader::LoadScene(System.Int32)
extern void SceneLoader_LoadScene_m52CC0D044A8AB6655C06F870DD8FC071DE0F8875 (void);
// 0x00000024 System.Void SceneLoader::MessengerFlutter()
extern void SceneLoader_MessengerFlutter_mD72284AC6111B4DD6761AC527423C3C0AC88CA0B (void);
// 0x00000025 System.Void SceneLoader::SwitchNative()
extern void SceneLoader_SwitchNative_m68C165016C6A7B1F29960507F65455523EA3A5F8 (void);
// 0x00000026 System.Void SceneLoader::UnloadNative()
extern void SceneLoader_UnloadNative_mA699427D00E7DD75BD5EFA4B0D54AFE5A0AFEACA (void);
// 0x00000027 System.Void SceneLoader::QuitNative()
extern void SceneLoader_QuitNative_mFE5B0B163F9D560A10ADB3574AFE755BAF6BFA43 (void);
// 0x00000028 System.Void SceneLoader::.ctor()
extern void SceneLoader__ctor_m2248766DF38AF07562AD31501C7275B8DF1B7D29 (void);
// 0x00000029 System.Void RC_Change_Color::Start()
extern void RC_Change_Color_Start_mC1EADEA2BAB694DA1CA925F071B4AC6BAD45F729 (void);
// 0x0000002A System.Void RC_Change_Color::ColorRed()
extern void RC_Change_Color_ColorRed_m791A5CFBB708D11F313CB5BD52504E661561AC48 (void);
// 0x0000002B System.Void RC_Change_Color::ColorGreen()
extern void RC_Change_Color_ColorGreen_mBFD39434C421292BEE728718BFCDF7FA78886C09 (void);
// 0x0000002C System.Void RC_Change_Color::ColorBlue()
extern void RC_Change_Color_ColorBlue_mC86A8A367658EE8AF61EF3613CCA0A614D6B8A66 (void);
// 0x0000002D System.Void RC_Change_Color::.ctor()
extern void RC_Change_Color__ctor_m03933BB8980F9336AE83FE5D1B79B095EA678BEB (void);
// 0x0000002E System.Void DeviceOrientation::Start()
extern void DeviceOrientation_Start_m77CA232FC1ED644713A63A08E71DE9B3F56AD185 (void);
// 0x0000002F System.Void DeviceOrientation::Update()
extern void DeviceOrientation_Update_mB1ED9AE216E1CD3A1000A68731C2B61FB0CEF244 (void);
// 0x00000030 System.Void DeviceOrientation::.ctor()
extern void DeviceOrientation__ctor_m269D2179C9F42E5A31173BDB099AD8A57AA1AC66 (void);
// 0x00000031 System.Void RC_Get_Texture::Start()
extern void RC_Get_Texture_Start_m9542714415E35F61FA7106594F62742C70621EF9 (void);
// 0x00000032 System.Void RC_Get_Texture::Update()
extern void RC_Get_Texture_Update_m2FD197E615B3501296A7A2A1C580B2D8011FDFBC (void);
// 0x00000033 System.Void RC_Get_Texture::onGUI()
extern void RC_Get_Texture_onGUI_mD13A0E9909C6191115EA3FADE17F8F93E11621F7 (void);
// 0x00000034 System.Void RC_Get_Texture::.ctor()
extern void RC_Get_Texture__ctor_mAE04EB7B457A5319FB58A91BEFE35BE12087E273 (void);
// 0x00000035 System.Void RegionCapture::OnApplicationFocus(System.Boolean)
extern void RegionCapture_OnApplicationFocus_m246F297F321C2C58C119FD6176C4C10AC90B4B50 (void);
// 0x00000036 System.Collections.IEnumerator RegionCapture::Start()
extern void RegionCapture_Start_m749401B6FC9AC307272775DA664B2818800CDEC2 (void);
// 0x00000037 System.Collections.IEnumerator RegionCapture::Initialize()
extern void RegionCapture_Initialize_mB33B40E3C3B607AA444DE9ECB78651CA2BB7AA4B (void);
// 0x00000038 System.Void RegionCapture::RCPreRender(UnityEngine.Camera)
extern void RegionCapture_RCPreRender_mDD46DA1598459C5C2BD5974502F47D1CF02DEA6E (void);
// 0x00000039 System.Void RegionCapture::OnEnable()
extern void RegionCapture_OnEnable_m50E487AB6CEF93D17404E4C0C9AF0C656A59047B (void);
// 0x0000003A System.Void RegionCapture::OnDisable()
extern void RegionCapture_OnDisable_mBD4D18ADF9F96E59807B71DBA793BD5F445DCAC8 (void);
// 0x0000003B System.Collections.IEnumerator RegionCapture::ResetYUVTextures()
extern void RegionCapture_ResetYUVTextures_m79EB1D9FABEC80027F9B52ACE42E07393E07BC76 (void);
// 0x0000003C System.Void RegionCapture::GetYUVTextures()
extern void RegionCapture_GetYUVTextures_m9668B7B91D26886683A9FE3D8DE18CA12420DE94 (void);
// 0x0000003D System.Collections.IEnumerator RegionCapture::Check_StateLoop()
extern void RegionCapture_Check_StateLoop_m9F70F9347178176A688B6E87A07A235E545EDE8B (void);
// 0x0000003E System.Void RegionCapture::MeshUpdate()
extern void RegionCapture_MeshUpdate_m94D2F10D1F9D2D00E228B3E4376CDC0CE832D435 (void);
// 0x0000003F System.Void RegionCapture::On_OutOfBounds()
extern void RegionCapture_On_OutOfBounds_m2DB311549C3A6DD43B7D0CDB1F800D4D684D44CA (void);
// 0x00000040 System.Void RegionCapture::FindBackgroundPlaneBounds(UnityEngine.GameObject)
extern void RegionCapture_FindBackgroundPlaneBounds_mEA23ABE44A6A016FC876C263582C0A7D9FD4B21C (void);
// 0x00000041 System.Void RegionCapture::.ctor()
extern void RegionCapture__ctor_m06876E833F887A08ECAF38B3B2A28DDACF4BE3A3 (void);
// 0x00000042 System.Void RegionCapture/<Start>d__26::.ctor(System.Int32)
extern void U3CStartU3Ed__26__ctor_m107EA8D10014BFD707CBD7DB488E55B7D24B4F87 (void);
// 0x00000043 System.Void RegionCapture/<Start>d__26::System.IDisposable.Dispose()
extern void U3CStartU3Ed__26_System_IDisposable_Dispose_m8D36C48E6CDCA735F82049966683871CFA9EB317 (void);
// 0x00000044 System.Boolean RegionCapture/<Start>d__26::MoveNext()
extern void U3CStartU3Ed__26_MoveNext_mC2AE417AB29C5B7BD4916FB45138A3A3C269C17C (void);
// 0x00000045 System.Object RegionCapture/<Start>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7243B119E93649FFFA11761EE6D813DFB4A5CE20 (void);
// 0x00000046 System.Void RegionCapture/<Start>d__26::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__26_System_Collections_IEnumerator_Reset_m2522E739995F6C011C6634E807A1726F9DD8BD46 (void);
// 0x00000047 System.Object RegionCapture/<Start>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__26_System_Collections_IEnumerator_get_Current_m0CA5443CD71DB17A8A3529B39D30D43FB8A81DFB (void);
// 0x00000048 System.Void RegionCapture/<Initialize>d__27::.ctor(System.Int32)
extern void U3CInitializeU3Ed__27__ctor_m14F6C75A1A310F9120D5CF93415CE35DB915849C (void);
// 0x00000049 System.Void RegionCapture/<Initialize>d__27::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__27_System_IDisposable_Dispose_m2B6A1BB4339776CD1B771B9792ADB8EC046ED3ED (void);
// 0x0000004A System.Boolean RegionCapture/<Initialize>d__27::MoveNext()
extern void U3CInitializeU3Ed__27_MoveNext_m40661FDEFE5D23521F0E400C45432A10DB0239D0 (void);
// 0x0000004B System.Object RegionCapture/<Initialize>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB74924E74F0BF7929CB31B59E89782A0E763BEC7 (void);
// 0x0000004C System.Void RegionCapture/<Initialize>d__27::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__27_System_Collections_IEnumerator_Reset_mC0130A7B2477020CD4294BB9391DBF394E388F58 (void);
// 0x0000004D System.Object RegionCapture/<Initialize>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__27_System_Collections_IEnumerator_get_Current_m57EC9AF343D9DEA1B250A4C6AA645E9DE576580B (void);
// 0x0000004E System.Void RegionCapture/<ResetYUVTextures>d__31::.ctor(System.Int32)
extern void U3CResetYUVTexturesU3Ed__31__ctor_m39DD1E99AE4292C3D602436DB5F9EBCE3035B2D1 (void);
// 0x0000004F System.Void RegionCapture/<ResetYUVTextures>d__31::System.IDisposable.Dispose()
extern void U3CResetYUVTexturesU3Ed__31_System_IDisposable_Dispose_m9CB261086DE591AEE8EB5DD3FF2E33870A82718C (void);
// 0x00000050 System.Boolean RegionCapture/<ResetYUVTextures>d__31::MoveNext()
extern void U3CResetYUVTexturesU3Ed__31_MoveNext_mCD5D3CA7713A0A79200C19D321B8123284F7343F (void);
// 0x00000051 System.Object RegionCapture/<ResetYUVTextures>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetYUVTexturesU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3DBF7BE1C07C463F41246B86B8C3C3C9CC3F6E8 (void);
// 0x00000052 System.Void RegionCapture/<ResetYUVTextures>d__31::System.Collections.IEnumerator.Reset()
extern void U3CResetYUVTexturesU3Ed__31_System_Collections_IEnumerator_Reset_mFEC70D59746EF407DCF6678205E60500C1DE21E5 (void);
// 0x00000053 System.Object RegionCapture/<ResetYUVTextures>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CResetYUVTexturesU3Ed__31_System_Collections_IEnumerator_get_Current_m1CC67B1F2A44A5C89570A5343EEBE03F54D11BA6 (void);
// 0x00000054 System.Void RegionCapture/<Check_StateLoop>d__33::.ctor(System.Int32)
extern void U3CCheck_StateLoopU3Ed__33__ctor_mA5E4E8C812499E26E5DEE2B69623E06139F94DF2 (void);
// 0x00000055 System.Void RegionCapture/<Check_StateLoop>d__33::System.IDisposable.Dispose()
extern void U3CCheck_StateLoopU3Ed__33_System_IDisposable_Dispose_mA5203C59AD125E98FDAF697F623AFE4F9690A5C4 (void);
// 0x00000056 System.Boolean RegionCapture/<Check_StateLoop>d__33::MoveNext()
extern void U3CCheck_StateLoopU3Ed__33_MoveNext_mE6C633D62266BB186BDFCF9750E156D44ADA613D (void);
// 0x00000057 System.Object RegionCapture/<Check_StateLoop>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheck_StateLoopU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m025EBA7CCEF9B181E22DC0D39FDF23B3DAE6FC27 (void);
// 0x00000058 System.Void RegionCapture/<Check_StateLoop>d__33::System.Collections.IEnumerator.Reset()
extern void U3CCheck_StateLoopU3Ed__33_System_Collections_IEnumerator_Reset_mD9BB9F8D2A607166D60992A7C197B4022B322950 (void);
// 0x00000059 System.Object RegionCapture/<Check_StateLoop>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CCheck_StateLoopU3Ed__33_System_Collections_IEnumerator_get_Current_m7E9D7C5F06208114032263B34E40970CB0EF331F (void);
// 0x0000005A UnityEngine.RenderTexture RenderTextureCamera::GetRenderTexture()
extern void RenderTextureCamera_GetRenderTexture_m73BDED6C94B7BD6D3C4F4A77E0B6ED9F14D8CDE6 (void);
// 0x0000005B System.Void RenderTextureCamera::Start()
extern void RenderTextureCamera_Start_m093CE375F9F7AF5CBD61443F896FD7AA0318F7A3 (void);
// 0x0000005C System.Void RenderTextureCamera::StartRenderingToTexture()
extern void RenderTextureCamera_StartRenderingToTexture_m722172EFA9D9A457D72E388DFEB47433F0E23E18 (void);
// 0x0000005D System.Void RenderTextureCamera::MakeScreen()
extern void RenderTextureCamera_MakeScreen_m463C6589E66E5AB939CCF9EBC6A4ACE4AFF2C99D (void);
// 0x0000005E System.Collections.IEnumerator RenderTextureCamera::TakeScreen()
extern void RenderTextureCamera_TakeScreen_m51EF60A31CE5CE02C0B39174D02B6900A6BC5F7F (void);
// 0x0000005F System.String RenderTextureCamera::saveImg(System.Byte[])
extern void RenderTextureCamera_saveImg_m006D8A3F6D9B2552FA9B4B6B714807AA04B4C0C1 (void);
// 0x00000060 System.Void RenderTextureCamera::.ctor()
extern void RenderTextureCamera__ctor_m32A232F2E528B4C5A421DCE13A66F3EA8A6D4EDF (void);
// 0x00000061 System.Void RenderTextureCamera/<TakeScreen>d__10::.ctor(System.Int32)
extern void U3CTakeScreenU3Ed__10__ctor_m31B0E98D0BD71D8A584892C9DC3B24BED2D5F18C (void);
// 0x00000062 System.Void RenderTextureCamera/<TakeScreen>d__10::System.IDisposable.Dispose()
extern void U3CTakeScreenU3Ed__10_System_IDisposable_Dispose_m926066AFBA8FCD2667452BD1DD9B102B10A6EB8A (void);
// 0x00000063 System.Boolean RenderTextureCamera/<TakeScreen>d__10::MoveNext()
extern void U3CTakeScreenU3Ed__10_MoveNext_m8B2ECF0A67A8DBE9F94F658B92D0138D695BE3AD (void);
// 0x00000064 System.Object RenderTextureCamera/<TakeScreen>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTakeScreenU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2169FDA231173600210A956FF22AAF540B0FD9F2 (void);
// 0x00000065 System.Void RenderTextureCamera/<TakeScreen>d__10::System.Collections.IEnumerator.Reset()
extern void U3CTakeScreenU3Ed__10_System_Collections_IEnumerator_Reset_m8480A69155FFC3A6421359C0761C1E8C4063A78C (void);
// 0x00000066 System.Object RenderTextureCamera/<TakeScreen>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CTakeScreenU3Ed__10_System_Collections_IEnumerator_get_Current_m5A283D735CAA37A271C2DE2B447AE928CC562582 (void);
// 0x00000067 System.Void SetAutoOrthographicSize::Update()
extern void SetAutoOrthographicSize_Update_m597512E662FA3CE9A5A5AA00F756CAEC6884490E (void);
// 0x00000068 System.Void SetAutoOrthographicSize::.ctor()
extern void SetAutoOrthographicSize__ctor_m4F69ED81B2CC1B4578255C864AE907CE3F576580 (void);
// 0x00000069 System.Void RotationScript::Start()
extern void RotationScript_Start_mC1D6BA453CE954E38A468689A2C80C18687B5114 (void);
// 0x0000006A System.Void RotationScript::Update()
extern void RotationScript_Update_mA2523225CA642E63A76A43AC82933468CD2C6D35 (void);
// 0x0000006B System.Void RotationScript::.ctor()
extern void RotationScript__ctor_mFC2425AC8C36AC75FF897CC45CAECD45E1109B63 (void);
// 0x0000006C System.Void FlutterUnityIntegration.NativeAPI::OnUnityMessage(System.String)
extern void NativeAPI_OnUnityMessage_mDABDDE84371D02AD0BD46C667C824FE3CEDFC3A9 (void);
// 0x0000006D System.Void FlutterUnityIntegration.NativeAPI::OnUnitySceneLoaded(System.String,System.Int32,System.Boolean,System.Boolean)
extern void NativeAPI_OnUnitySceneLoaded_mAA3BA99C66FC80AE6A24B74B58353B10096310C1 (void);
// 0x0000006E System.Void FlutterUnityIntegration.NativeAPI::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void NativeAPI_OnSceneLoaded_m76DA0817E7294BB102C73B14910EE41293347A33 (void);
// 0x0000006F System.Void FlutterUnityIntegration.NativeAPI::SendMessageToFlutter(System.String)
extern void NativeAPI_SendMessageToFlutter_mC740B9FE81A08CE426AFBE00883156675A7FF81D (void);
// 0x00000070 System.Void FlutterUnityIntegration.NativeAPI::ShowHostMainWindow()
extern void NativeAPI_ShowHostMainWindow_m3798A75BE35C806C26EB80192FF7D967003E6EE3 (void);
// 0x00000071 System.Void FlutterUnityIntegration.NativeAPI::UnloadMainWindow()
extern void NativeAPI_UnloadMainWindow_m8D4AA21908132CD11384A4F7456AD4D048E5F4E8 (void);
// 0x00000072 System.Void FlutterUnityIntegration.NativeAPI::QuitUnityWindow()
extern void NativeAPI_QuitUnityWindow_m8FBDD8217158787D66E32F0DD1803B5B47D8B16F (void);
// 0x00000073 System.Void FlutterUnityIntegration.NativeAPI::.ctor()
extern void NativeAPI__ctor_mA061363778EC083B262B2B1F2350ED7B9AE97AD9 (void);
// 0x00000074 T FlutterUnityIntegration.SingletonMonoBehaviour`1::get_Instance()
// 0x00000075 T FlutterUnityIntegration.SingletonMonoBehaviour`1::CreateSingleton()
// 0x00000076 System.Void FlutterUnityIntegration.SingletonMonoBehaviour`1::.ctor()
// 0x00000077 System.Void FlutterUnityIntegration.SingletonMonoBehaviour`1::.cctor()
// 0x00000078 FlutterUnityIntegration.MessageHandler FlutterUnityIntegration.MessageHandler::Deserialize(System.String)
extern void MessageHandler_Deserialize_mC39AE13917392501C10100157C09CE30111A7CD7 (void);
// 0x00000079 T FlutterUnityIntegration.MessageHandler::getData()
// 0x0000007A System.Void FlutterUnityIntegration.MessageHandler::.ctor(System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JToken)
extern void MessageHandler__ctor_m1B476AF6138B8567F1AC1613B57F26882755D0AE (void);
// 0x0000007B System.Void FlutterUnityIntegration.MessageHandler::send(System.Object)
extern void MessageHandler_send_m46F4AB9D42028AEA5D5F11F6BB10B4490385E963 (void);
// 0x0000007C System.Void FlutterUnityIntegration.UnityMessage::.ctor()
extern void UnityMessage__ctor_mDEC55E5CFC43BE8B679F2B49B6245BD6DD3DE9FC (void);
// 0x0000007D System.Int32 FlutterUnityIntegration.UnityMessageManager::generateId()
extern void UnityMessageManager_generateId_m540CE5EE3D8D88166C8E507528F534CF771F8B61 (void);
// 0x0000007E System.Void FlutterUnityIntegration.UnityMessageManager::add_OnMessage(FlutterUnityIntegration.UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_add_OnMessage_m72A64B53769F3968F62E977712541F3EB6700CBD (void);
// 0x0000007F System.Void FlutterUnityIntegration.UnityMessageManager::remove_OnMessage(FlutterUnityIntegration.UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_remove_OnMessage_m7B30D079CD871DE3B1213E9E33179EEF595E48FD (void);
// 0x00000080 System.Void FlutterUnityIntegration.UnityMessageManager::add_OnFlutterMessage(FlutterUnityIntegration.UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_add_OnFlutterMessage_mA4F4A939F058D2172A994AA83878FC49F4A60D28 (void);
// 0x00000081 System.Void FlutterUnityIntegration.UnityMessageManager::remove_OnFlutterMessage(FlutterUnityIntegration.UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_remove_OnFlutterMessage_mE9773A6B3A6DEF366B0FA9668DAA27532C005925 (void);
// 0x00000082 System.Void FlutterUnityIntegration.UnityMessageManager::Start()
extern void UnityMessageManager_Start_m513A44342D5A5336DB3BB0AD0501525BC0A4F63D (void);
// 0x00000083 System.Void FlutterUnityIntegration.UnityMessageManager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void UnityMessageManager_OnSceneLoaded_m9C97092396F4D86D596E39FEB791D14B7A98A726 (void);
// 0x00000084 System.Void FlutterUnityIntegration.UnityMessageManager::ShowHostMainWindow()
extern void UnityMessageManager_ShowHostMainWindow_mDD2898A6D41376DEB7033193BD31AA946EEEF460 (void);
// 0x00000085 System.Void FlutterUnityIntegration.UnityMessageManager::UnloadMainWindow()
extern void UnityMessageManager_UnloadMainWindow_m1EC3A84E3AD1C76D7915A5DD44597E60CB4CF1FD (void);
// 0x00000086 System.Void FlutterUnityIntegration.UnityMessageManager::QuitUnityWindow()
extern void UnityMessageManager_QuitUnityWindow_m15E0FC94CB42CDA85229509AD473E2BE0E27762F (void);
// 0x00000087 System.Void FlutterUnityIntegration.UnityMessageManager::SendMessageToFlutter(System.String)
extern void UnityMessageManager_SendMessageToFlutter_m01236AF27F51C863E80F5220F02BA45E9ED4BAB9 (void);
// 0x00000088 System.Void FlutterUnityIntegration.UnityMessageManager::SendMessageToFlutter(FlutterUnityIntegration.UnityMessage)
extern void UnityMessageManager_SendMessageToFlutter_m5258B3CDED96288FA463D52AD7DF4D392674A5E0 (void);
// 0x00000089 System.Void FlutterUnityIntegration.UnityMessageManager::onMessage(System.String)
extern void UnityMessageManager_onMessage_mA23F949C1E38D874363AF6EFC0B9D7F7ABC9BD19 (void);
// 0x0000008A System.Void FlutterUnityIntegration.UnityMessageManager::onFlutterMessage(System.String)
extern void UnityMessageManager_onFlutterMessage_m5075BDBB0B6A49C5FB47DFCAFD55D1267BF5E3B5 (void);
// 0x0000008B System.Void FlutterUnityIntegration.UnityMessageManager::.ctor()
extern void UnityMessageManager__ctor_m33386834F5F300E49185600FC190C57D3E9C0233 (void);
// 0x0000008C System.Void FlutterUnityIntegration.UnityMessageManager/MessageDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageDelegate__ctor_m1309BD3DE28E87C0CC24EB097613F916E8E4A1DB (void);
// 0x0000008D System.Void FlutterUnityIntegration.UnityMessageManager/MessageDelegate::Invoke(System.String)
extern void MessageDelegate_Invoke_m42A129A125C34AB085FEBE1CC9B5984ACC1BB7C6 (void);
// 0x0000008E System.IAsyncResult FlutterUnityIntegration.UnityMessageManager/MessageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MessageDelegate_BeginInvoke_mBBFD55486390BFD20AD687A88C02D1C554C6A3E3 (void);
// 0x0000008F System.Void FlutterUnityIntegration.UnityMessageManager/MessageDelegate::EndInvoke(System.IAsyncResult)
extern void MessageDelegate_EndInvoke_m1C05B416E71A364B4560BA5085ECBBAF6BF87E97 (void);
// 0x00000090 System.Void FlutterUnityIntegration.UnityMessageManager/MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageHandlerDelegate__ctor_m287CBD25D77011FC5A52D1090F6CC31353AB6364 (void);
// 0x00000091 System.Void FlutterUnityIntegration.UnityMessageManager/MessageHandlerDelegate::Invoke(FlutterUnityIntegration.MessageHandler)
extern void MessageHandlerDelegate_Invoke_mD1EC9E1841D5014C40A26D4C4BA076482FFC8A4B (void);
// 0x00000092 System.IAsyncResult FlutterUnityIntegration.UnityMessageManager/MessageHandlerDelegate::BeginInvoke(FlutterUnityIntegration.MessageHandler,System.AsyncCallback,System.Object)
extern void MessageHandlerDelegate_BeginInvoke_m0CC5C5B29E11DD03F1726B49E4B1E516479AC58D (void);
// 0x00000093 System.Void FlutterUnityIntegration.UnityMessageManager/MessageHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void MessageHandlerDelegate_EndInvoke_mAD2746E001DB9A3D18A1D806610542321FE0CF16 (void);
static Il2CppMethodPointer s_methodPointers[147] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AnchorCreator_get_AnchorPrefab_mBBAF7805E9D9F8D79408EE642D153BC76306B25A,
	AnchorCreator_set_AnchorPrefab_m53CC9CC3022C713826B44536B48B462C8A1FACFF,
	AnchorCreator_RemoveAllAnchors_m6BA302DD9EECA47969FD3F6E5397441DCB09ED21,
	AnchorCreator_Awake_m8820A3F157354D7EDED7B2D7CE2BFA42844F1F8E,
	AnchorCreator_Update_mA0A8BCCBAB0AE50DB087524E6273D11F1D6456D0,
	AnchorCreator__ctor_m702B01425680D7FD1C9272A423AAF8A913E5476E,
	AnchorCreator__cctor_mF5AE519F086E52A01EA091115062D84D9F094F34,
	ARFeatheredPlaneMeshVisualizer_get_featheringWidth_m14D3A8BE3E9A745E6FD525B19ADDC892B8399B4D,
	ARFeatheredPlaneMeshVisualizer_set_featheringWidth_mD616A09A3B426EA5DE1FA37334DD194E43EEC110,
	ARFeatheredPlaneMeshVisualizer_Awake_mC5DB0414A2514BF4851266C25141C903F0AC57BA,
	ARFeatheredPlaneMeshVisualizer_OnEnable_m8781C85CFED871C8A81A5B88DB1031856E0FC9F0,
	ARFeatheredPlaneMeshVisualizer_OnDisable_m2343B05B1A8F14BAD4DD516C584281B66FE6A4E8,
	ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_mB3D9BBD14EA1FE3ECDBACC2DB89C1B110B8B6B5F,
	ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_mF756D3C1F7925A69CD8C7C8CCE56209AB321FEF5,
	ARFeatheredPlaneMeshVisualizer__ctor_m9A77651BCAE58AA0B994FFF6C6B63B1CFF2729F6,
	ARFeatheredPlaneMeshVisualizer__cctor_mF4BA6DDB611A3FD966C8B9494AE6B3EB8647CEAD,
	GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2,
	GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41,
	GameManager_HandleWebFnCall_m376ED43FA51805EF23D17DD050E5B73897EC6185,
	GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368,
	Rotate_Start_mD322E77A3CF2BEF28C4DF71D3F529107F511B1FB,
	Rotate_Update_m73D585515036D9B7AAD8336BFB8567283CE4C7E7,
	Rotate_SetRotationSpeed_mD754DB628A79A7B2ADAED25DE732A29E7548F2F1,
	Rotate__ctor_m0EE5CC8EB699542BFC438DC3D547D39E442E9EE4,
	SceneLoader_Start_mB9AA9E8ADCE59F893E3EF8E891ED5E1F3AB80DA0,
	SceneLoader_Update_m1F381D68B1B1F69A0F9A5876813BB7D437E3A713,
	SceneLoader_LoadScene_m52CC0D044A8AB6655C06F870DD8FC071DE0F8875,
	SceneLoader_MessengerFlutter_mD72284AC6111B4DD6761AC527423C3C0AC88CA0B,
	SceneLoader_SwitchNative_m68C165016C6A7B1F29960507F65455523EA3A5F8,
	SceneLoader_UnloadNative_mA699427D00E7DD75BD5EFA4B0D54AFE5A0AFEACA,
	SceneLoader_QuitNative_mFE5B0B163F9D560A10ADB3574AFE755BAF6BFA43,
	SceneLoader__ctor_m2248766DF38AF07562AD31501C7275B8DF1B7D29,
	RC_Change_Color_Start_mC1EADEA2BAB694DA1CA925F071B4AC6BAD45F729,
	RC_Change_Color_ColorRed_m791A5CFBB708D11F313CB5BD52504E661561AC48,
	RC_Change_Color_ColorGreen_mBFD39434C421292BEE728718BFCDF7FA78886C09,
	RC_Change_Color_ColorBlue_mC86A8A367658EE8AF61EF3613CCA0A614D6B8A66,
	RC_Change_Color__ctor_m03933BB8980F9336AE83FE5D1B79B095EA678BEB,
	DeviceOrientation_Start_m77CA232FC1ED644713A63A08E71DE9B3F56AD185,
	DeviceOrientation_Update_mB1ED9AE216E1CD3A1000A68731C2B61FB0CEF244,
	DeviceOrientation__ctor_m269D2179C9F42E5A31173BDB099AD8A57AA1AC66,
	RC_Get_Texture_Start_m9542714415E35F61FA7106594F62742C70621EF9,
	RC_Get_Texture_Update_m2FD197E615B3501296A7A2A1C580B2D8011FDFBC,
	RC_Get_Texture_onGUI_mD13A0E9909C6191115EA3FADE17F8F93E11621F7,
	RC_Get_Texture__ctor_mAE04EB7B457A5319FB58A91BEFE35BE12087E273,
	RegionCapture_OnApplicationFocus_m246F297F321C2C58C119FD6176C4C10AC90B4B50,
	RegionCapture_Start_m749401B6FC9AC307272775DA664B2818800CDEC2,
	RegionCapture_Initialize_mB33B40E3C3B607AA444DE9ECB78651CA2BB7AA4B,
	RegionCapture_RCPreRender_mDD46DA1598459C5C2BD5974502F47D1CF02DEA6E,
	RegionCapture_OnEnable_m50E487AB6CEF93D17404E4C0C9AF0C656A59047B,
	RegionCapture_OnDisable_mBD4D18ADF9F96E59807B71DBA793BD5F445DCAC8,
	RegionCapture_ResetYUVTextures_m79EB1D9FABEC80027F9B52ACE42E07393E07BC76,
	RegionCapture_GetYUVTextures_m9668B7B91D26886683A9FE3D8DE18CA12420DE94,
	RegionCapture_Check_StateLoop_m9F70F9347178176A688B6E87A07A235E545EDE8B,
	RegionCapture_MeshUpdate_m94D2F10D1F9D2D00E228B3E4376CDC0CE832D435,
	RegionCapture_On_OutOfBounds_m2DB311549C3A6DD43B7D0CDB1F800D4D684D44CA,
	RegionCapture_FindBackgroundPlaneBounds_mEA23ABE44A6A016FC876C263582C0A7D9FD4B21C,
	RegionCapture__ctor_m06876E833F887A08ECAF38B3B2A28DDACF4BE3A3,
	U3CStartU3Ed__26__ctor_m107EA8D10014BFD707CBD7DB488E55B7D24B4F87,
	U3CStartU3Ed__26_System_IDisposable_Dispose_m8D36C48E6CDCA735F82049966683871CFA9EB317,
	U3CStartU3Ed__26_MoveNext_mC2AE417AB29C5B7BD4916FB45138A3A3C269C17C,
	U3CStartU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7243B119E93649FFFA11761EE6D813DFB4A5CE20,
	U3CStartU3Ed__26_System_Collections_IEnumerator_Reset_m2522E739995F6C011C6634E807A1726F9DD8BD46,
	U3CStartU3Ed__26_System_Collections_IEnumerator_get_Current_m0CA5443CD71DB17A8A3529B39D30D43FB8A81DFB,
	U3CInitializeU3Ed__27__ctor_m14F6C75A1A310F9120D5CF93415CE35DB915849C,
	U3CInitializeU3Ed__27_System_IDisposable_Dispose_m2B6A1BB4339776CD1B771B9792ADB8EC046ED3ED,
	U3CInitializeU3Ed__27_MoveNext_m40661FDEFE5D23521F0E400C45432A10DB0239D0,
	U3CInitializeU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB74924E74F0BF7929CB31B59E89782A0E763BEC7,
	U3CInitializeU3Ed__27_System_Collections_IEnumerator_Reset_mC0130A7B2477020CD4294BB9391DBF394E388F58,
	U3CInitializeU3Ed__27_System_Collections_IEnumerator_get_Current_m57EC9AF343D9DEA1B250A4C6AA645E9DE576580B,
	U3CResetYUVTexturesU3Ed__31__ctor_m39DD1E99AE4292C3D602436DB5F9EBCE3035B2D1,
	U3CResetYUVTexturesU3Ed__31_System_IDisposable_Dispose_m9CB261086DE591AEE8EB5DD3FF2E33870A82718C,
	U3CResetYUVTexturesU3Ed__31_MoveNext_mCD5D3CA7713A0A79200C19D321B8123284F7343F,
	U3CResetYUVTexturesU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3DBF7BE1C07C463F41246B86B8C3C3C9CC3F6E8,
	U3CResetYUVTexturesU3Ed__31_System_Collections_IEnumerator_Reset_mFEC70D59746EF407DCF6678205E60500C1DE21E5,
	U3CResetYUVTexturesU3Ed__31_System_Collections_IEnumerator_get_Current_m1CC67B1F2A44A5C89570A5343EEBE03F54D11BA6,
	U3CCheck_StateLoopU3Ed__33__ctor_mA5E4E8C812499E26E5DEE2B69623E06139F94DF2,
	U3CCheck_StateLoopU3Ed__33_System_IDisposable_Dispose_mA5203C59AD125E98FDAF697F623AFE4F9690A5C4,
	U3CCheck_StateLoopU3Ed__33_MoveNext_mE6C633D62266BB186BDFCF9750E156D44ADA613D,
	U3CCheck_StateLoopU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m025EBA7CCEF9B181E22DC0D39FDF23B3DAE6FC27,
	U3CCheck_StateLoopU3Ed__33_System_Collections_IEnumerator_Reset_mD9BB9F8D2A607166D60992A7C197B4022B322950,
	U3CCheck_StateLoopU3Ed__33_System_Collections_IEnumerator_get_Current_m7E9D7C5F06208114032263B34E40970CB0EF331F,
	RenderTextureCamera_GetRenderTexture_m73BDED6C94B7BD6D3C4F4A77E0B6ED9F14D8CDE6,
	RenderTextureCamera_Start_m093CE375F9F7AF5CBD61443F896FD7AA0318F7A3,
	RenderTextureCamera_StartRenderingToTexture_m722172EFA9D9A457D72E388DFEB47433F0E23E18,
	RenderTextureCamera_MakeScreen_m463C6589E66E5AB939CCF9EBC6A4ACE4AFF2C99D,
	RenderTextureCamera_TakeScreen_m51EF60A31CE5CE02C0B39174D02B6900A6BC5F7F,
	RenderTextureCamera_saveImg_m006D8A3F6D9B2552FA9B4B6B714807AA04B4C0C1,
	RenderTextureCamera__ctor_m32A232F2E528B4C5A421DCE13A66F3EA8A6D4EDF,
	U3CTakeScreenU3Ed__10__ctor_m31B0E98D0BD71D8A584892C9DC3B24BED2D5F18C,
	U3CTakeScreenU3Ed__10_System_IDisposable_Dispose_m926066AFBA8FCD2667452BD1DD9B102B10A6EB8A,
	U3CTakeScreenU3Ed__10_MoveNext_m8B2ECF0A67A8DBE9F94F658B92D0138D695BE3AD,
	U3CTakeScreenU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2169FDA231173600210A956FF22AAF540B0FD9F2,
	U3CTakeScreenU3Ed__10_System_Collections_IEnumerator_Reset_m8480A69155FFC3A6421359C0761C1E8C4063A78C,
	U3CTakeScreenU3Ed__10_System_Collections_IEnumerator_get_Current_m5A283D735CAA37A271C2DE2B447AE928CC562582,
	SetAutoOrthographicSize_Update_m597512E662FA3CE9A5A5AA00F756CAEC6884490E,
	SetAutoOrthographicSize__ctor_m4F69ED81B2CC1B4578255C864AE907CE3F576580,
	RotationScript_Start_mC1D6BA453CE954E38A468689A2C80C18687B5114,
	RotationScript_Update_mA2523225CA642E63A76A43AC82933468CD2C6D35,
	RotationScript__ctor_mFC2425AC8C36AC75FF897CC45CAECD45E1109B63,
	NativeAPI_OnUnityMessage_mDABDDE84371D02AD0BD46C667C824FE3CEDFC3A9,
	NativeAPI_OnUnitySceneLoaded_mAA3BA99C66FC80AE6A24B74B58353B10096310C1,
	NativeAPI_OnSceneLoaded_m76DA0817E7294BB102C73B14910EE41293347A33,
	NativeAPI_SendMessageToFlutter_mC740B9FE81A08CE426AFBE00883156675A7FF81D,
	NativeAPI_ShowHostMainWindow_m3798A75BE35C806C26EB80192FF7D967003E6EE3,
	NativeAPI_UnloadMainWindow_m8D4AA21908132CD11384A4F7456AD4D048E5F4E8,
	NativeAPI_QuitUnityWindow_m8FBDD8217158787D66E32F0DD1803B5B47D8B16F,
	NativeAPI__ctor_mA061363778EC083B262B2B1F2350ED7B9AE97AD9,
	NULL,
	NULL,
	NULL,
	NULL,
	MessageHandler_Deserialize_mC39AE13917392501C10100157C09CE30111A7CD7,
	NULL,
	MessageHandler__ctor_m1B476AF6138B8567F1AC1613B57F26882755D0AE,
	MessageHandler_send_m46F4AB9D42028AEA5D5F11F6BB10B4490385E963,
	UnityMessage__ctor_mDEC55E5CFC43BE8B679F2B49B6245BD6DD3DE9FC,
	UnityMessageManager_generateId_m540CE5EE3D8D88166C8E507528F534CF771F8B61,
	UnityMessageManager_add_OnMessage_m72A64B53769F3968F62E977712541F3EB6700CBD,
	UnityMessageManager_remove_OnMessage_m7B30D079CD871DE3B1213E9E33179EEF595E48FD,
	UnityMessageManager_add_OnFlutterMessage_mA4F4A939F058D2172A994AA83878FC49F4A60D28,
	UnityMessageManager_remove_OnFlutterMessage_mE9773A6B3A6DEF366B0FA9668DAA27532C005925,
	UnityMessageManager_Start_m513A44342D5A5336DB3BB0AD0501525BC0A4F63D,
	UnityMessageManager_OnSceneLoaded_m9C97092396F4D86D596E39FEB791D14B7A98A726,
	UnityMessageManager_ShowHostMainWindow_mDD2898A6D41376DEB7033193BD31AA946EEEF460,
	UnityMessageManager_UnloadMainWindow_m1EC3A84E3AD1C76D7915A5DD44597E60CB4CF1FD,
	UnityMessageManager_QuitUnityWindow_m15E0FC94CB42CDA85229509AD473E2BE0E27762F,
	UnityMessageManager_SendMessageToFlutter_m01236AF27F51C863E80F5220F02BA45E9ED4BAB9,
	UnityMessageManager_SendMessageToFlutter_m5258B3CDED96288FA463D52AD7DF4D392674A5E0,
	UnityMessageManager_onMessage_mA23F949C1E38D874363AF6EFC0B9D7F7ABC9BD19,
	UnityMessageManager_onFlutterMessage_m5075BDBB0B6A49C5FB47DFCAFD55D1267BF5E3B5,
	UnityMessageManager__ctor_m33386834F5F300E49185600FC190C57D3E9C0233,
	MessageDelegate__ctor_m1309BD3DE28E87C0CC24EB097613F916E8E4A1DB,
	MessageDelegate_Invoke_m42A129A125C34AB085FEBE1CC9B5984ACC1BB7C6,
	MessageDelegate_BeginInvoke_mBBFD55486390BFD20AD687A88C02D1C554C6A3E3,
	MessageDelegate_EndInvoke_m1C05B416E71A364B4560BA5085ECBBAF6BF87E97,
	MessageHandlerDelegate__ctor_m287CBD25D77011FC5A52D1090F6CC31353AB6364,
	MessageHandlerDelegate_Invoke_mD1EC9E1841D5014C40A26D4C4BA076482FFC8A4B,
	MessageHandlerDelegate_BeginInvoke_m0CC5C5B29E11DD03F1726B49E4B1E516479AC58D,
	MessageHandlerDelegate_EndInvoke_mAD2746E001DB9A3D18A1D806610542321FE0CF16,
};
static const int32_t s_InvokerIndices[147] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	7147,
	5698,
	7271,
	7271,
	7271,
	7271,
	11439,
	7199,
	5740,
	7271,
	7271,
	7271,
	5568,
	5698,
	7271,
	11439,
	7271,
	7271,
	5698,
	7271,
	7271,
	7271,
	5698,
	7271,
	7271,
	7271,
	5661,
	7271,
	7271,
	7271,
	7271,
	7271,
	7271,
	7271,
	7271,
	7271,
	7271,
	7271,
	7271,
	7271,
	7271,
	7271,
	7271,
	7271,
	5591,
	7147,
	7147,
	5698,
	7271,
	7271,
	7147,
	7271,
	7147,
	7271,
	7271,
	5698,
	7271,
	5661,
	7271,
	7036,
	7147,
	7271,
	7147,
	5661,
	7271,
	7036,
	7147,
	7271,
	7147,
	5661,
	7271,
	7036,
	7147,
	7271,
	7147,
	5661,
	7271,
	7036,
	7147,
	7271,
	7147,
	7147,
	7271,
	7271,
	7271,
	7147,
	5011,
	7271,
	5661,
	7271,
	7036,
	7147,
	7271,
	7147,
	7271,
	7271,
	7271,
	7271,
	7271,
	11182,
	8654,
	10240,
	11182,
	11439,
	11439,
	11439,
	7271,
	0,
	0,
	0,
	0,
	10900,
	0,
	1092,
	5698,
	7271,
	11388,
	5698,
	5698,
	5698,
	5698,
	7271,
	3140,
	7271,
	7271,
	7271,
	5698,
	5698,
	5698,
	5698,
	7271,
	3083,
	5698,
	1417,
	5698,
	3083,
	5698,
	1417,
	5698,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000002, { 0, 29 } },
	{ 0x02000015, { 29, 9 } },
	{ 0x06000079, { 38, 1 } },
};
extern const uint32_t g_rgctx_U3CU3Ef__AnonymousType0_4_t93FD0713D40011506D6B5581D9D5064D1A1A31B8;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_mCBE753640994A70A4B0EB9A431EEC6EC666C3927;
extern const uint32_t g_rgctx_EqualityComparer_1_tE2981F42233FBD735ACCD8037D75D3BB4CD17AB4;
extern const uint32_t g_rgctx_EqualityComparer_1_tE2981F42233FBD735ACCD8037D75D3BB4CD17AB4;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m3A73853DF38D7A5F7435032EADA0E70E576A57E9;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_m539382C7982D9E2955F588CA23AE8EF5AFC5848B;
extern const uint32_t g_rgctx_EqualityComparer_1_tAD002BC32A9F45FB9ADC01B444A699824C79C189;
extern const uint32_t g_rgctx_EqualityComparer_1_tAD002BC32A9F45FB9ADC01B444A699824C79C189;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m87448C8C6392FA740FB5A3A3B9A4FCB16896BDEE;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_m73C9E5DCDC640EC4F088E091D50B61E296387009;
extern const uint32_t g_rgctx_EqualityComparer_1_t86248AA917021F43C77C70906218B4C5685ED26B;
extern const uint32_t g_rgctx_EqualityComparer_1_t86248AA917021F43C77C70906218B4C5685ED26B;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_mEBE02056FDBB2ECEBEFCA1A556F823D3F4040362;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_mDFE711273CB32460A725376F86D0DCFEA7BE3C66;
extern const uint32_t g_rgctx_EqualityComparer_1_tD053F5CE6551182CE911D8C3CE2540B97F156987;
extern const uint32_t g_rgctx_EqualityComparer_1_tD053F5CE6551182CE911D8C3CE2540B97F156987;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m6BA30BEAA0DF6985F9B87C8479952DB424D8241B;
extern const uint32_t g_rgctx_EqualityComparer_1_GetHashCode_mAEF4D26E612F6E42ECFBEAFDF77767D2988E1619;
extern const uint32_t g_rgctx_EqualityComparer_1_GetHashCode_m6089000CD165B6ABC68857AA438A3950CDCC5B43;
extern const uint32_t g_rgctx_EqualityComparer_1_GetHashCode_mBCF474B9011C058E0570312097A7F34C09C33DF6;
extern const uint32_t g_rgctx_EqualityComparer_1_GetHashCode_mB641ECF38D3923819EB55D5F12C5B16C3264E1A1;
extern const uint32_t g_rgctx_U3CidU3Ej__TPar_tB4283D6614B0A1B9713411CF590AE6445EC76C76;
extern const Il2CppRGCTXConstrainedData g_rgctx_U3CidU3Ej__TPar_tB4283D6614B0A1B9713411CF590AE6445EC76C76_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_U3CseqU3Ej__TPar_t5290712E6748D411D87B0461BCF3FCF7A8412B71;
extern const Il2CppRGCTXConstrainedData g_rgctx_U3CseqU3Ej__TPar_t5290712E6748D411D87B0461BCF3FCF7A8412B71_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_U3CnameU3Ej__TPar_tF038DFA17A672607E28AD9AB1F07F3C69402F7F1;
extern const Il2CppRGCTXConstrainedData g_rgctx_U3CnameU3Ej__TPar_tF038DFA17A672607E28AD9AB1F07F3C69402F7F1_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_U3CdataU3Ej__TPar_tA1CCD24F7E35C5B61B29E317B30895EC9D15768F;
extern const Il2CppRGCTXConstrainedData g_rgctx_U3CdataU3Ej__TPar_tA1CCD24F7E35C5B61B29E317B30895EC9D15768F_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_SingletonMonoBehaviour_1_tA810EBDF6C08172FBA622880B48E367733A06C19;
extern const uint32_t g_rgctx_Lazy_1_t145603A39FBB008300940532C3BA916A1DE95F34;
extern const uint32_t g_rgctx_Lazy_1_get_Value_m7DE2E25A3C0093ABC923AF98ABBF9F8E7E0DCCD9;
extern const uint32_t g_rgctx_T_t949FD4146F88D5E97C833150021CC2D3F184D911;
extern const uint32_t g_rgctx_GameObject_AddComponent_TisT_t949FD4146F88D5E97C833150021CC2D3F184D911_m385B14DEEEBF28C3156F54753EAC8EB0F59867D1;
extern const uint32_t g_rgctx_SingletonMonoBehaviour_1_CreateSingleton_mF8DB405D4982AAEDAB492FCD5C1DA512DF216784;
extern const uint32_t g_rgctx_Func_1_t96038924E1024CCBE9198667A36B2B43C6B25327;
extern const uint32_t g_rgctx_Func_1__ctor_mAD3E54C1E059EDDDE1113386F6BA060C9010C09E;
extern const uint32_t g_rgctx_Lazy_1__ctor_m0361BDDA1494BE9B73D13448F42C6967A5C804E1;
extern const uint32_t g_rgctx_Extensions_Value_TisT_tEFC8830F1FE6E05437F90CF7FEE659B3D392EC0D_m7D42C3E3645F2DA8E1D763AEFE6E143ACB076FE3;
static const Il2CppRGCTXDefinition s_rgctxValues[39] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ef__AnonymousType0_4_t93FD0713D40011506D6B5581D9D5064D1A1A31B8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_mCBE753640994A70A4B0EB9A431EEC6EC666C3927 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tE2981F42233FBD735ACCD8037D75D3BB4CD17AB4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tE2981F42233FBD735ACCD8037D75D3BB4CD17AB4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m3A73853DF38D7A5F7435032EADA0E70E576A57E9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_m539382C7982D9E2955F588CA23AE8EF5AFC5848B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tAD002BC32A9F45FB9ADC01B444A699824C79C189 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tAD002BC32A9F45FB9ADC01B444A699824C79C189 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m87448C8C6392FA740FB5A3A3B9A4FCB16896BDEE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_m73C9E5DCDC640EC4F088E091D50B61E296387009 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t86248AA917021F43C77C70906218B4C5685ED26B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t86248AA917021F43C77C70906218B4C5685ED26B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_mEBE02056FDBB2ECEBEFCA1A556F823D3F4040362 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_mDFE711273CB32460A725376F86D0DCFEA7BE3C66 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tD053F5CE6551182CE911D8C3CE2540B97F156987 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tD053F5CE6551182CE911D8C3CE2540B97F156987 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m6BA30BEAA0DF6985F9B87C8479952DB424D8241B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_GetHashCode_mAEF4D26E612F6E42ECFBEAFDF77767D2988E1619 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_GetHashCode_m6089000CD165B6ABC68857AA438A3950CDCC5B43 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_GetHashCode_mBCF474B9011C058E0570312097A7F34C09C33DF6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_GetHashCode_mB641ECF38D3923819EB55D5F12C5B16C3264E1A1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CidU3Ej__TPar_tB4283D6614B0A1B9713411CF590AE6445EC76C76 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_U3CidU3Ej__TPar_tB4283D6614B0A1B9713411CF590AE6445EC76C76_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CseqU3Ej__TPar_t5290712E6748D411D87B0461BCF3FCF7A8412B71 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_U3CseqU3Ej__TPar_t5290712E6748D411D87B0461BCF3FCF7A8412B71_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CnameU3Ej__TPar_tF038DFA17A672607E28AD9AB1F07F3C69402F7F1 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_U3CnameU3Ej__TPar_tF038DFA17A672607E28AD9AB1F07F3C69402F7F1_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CdataU3Ej__TPar_tA1CCD24F7E35C5B61B29E317B30895EC9D15768F },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_U3CdataU3Ej__TPar_tA1CCD24F7E35C5B61B29E317B30895EC9D15768F_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SingletonMonoBehaviour_1_tA810EBDF6C08172FBA622880B48E367733A06C19 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Lazy_1_t145603A39FBB008300940532C3BA916A1DE95F34 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Lazy_1_get_Value_m7DE2E25A3C0093ABC923AF98ABBF9F8E7E0DCCD9 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t949FD4146F88D5E97C833150021CC2D3F184D911 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_GameObject_AddComponent_TisT_t949FD4146F88D5E97C833150021CC2D3F184D911_m385B14DEEEBF28C3156F54753EAC8EB0F59867D1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SingletonMonoBehaviour_1_CreateSingleton_mF8DB405D4982AAEDAB492FCD5C1DA512DF216784 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t96038924E1024CCBE9198667A36B2B43C6B25327 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1__ctor_mAD3E54C1E059EDDDE1113386F6BA060C9010C09E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Lazy_1__ctor_m0361BDDA1494BE9B73D13448F42C6967A5C804E1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Extensions_Value_TisT_tEFC8830F1FE6E05437F90CF7FEE659B3D392EC0D_m7D42C3E3645F2DA8E1D763AEFE6E143ACB076FE3 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	147,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	39,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
