﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtualFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Text.Encoding>
struct Dictionary_2_t87EDE08B2E48F793A22DE50D6B3CC2E7EBB2DB54;
// System.Collections.Generic.List`1<System.String>
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0;
// System.AsyncCallback
struct AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C;
// Assets.Scripts.CRMLink
struct CRMLink_t54EC67907837D04A6A426F71A41123CB7A8E5778;
// Assets.Scripts.CRMRequest
struct CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B;
// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t52460FA30AE37F4F26ACB81055E58002262F19F2;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B;
// System.Text.DecoderFallback
struct DecoderFallback_t7324102215E4ED41EC065C02EB501CB0BC23CD90;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB;
// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974;
// System.Text.EncoderFallback
struct EncoderFallback_tD2C40CE114AA9D8E1F7196608B2D088548015293;
// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095;
// UnityEngine.UI.FontData
struct FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// System.IAsyncResult
struct IAsyncResult_t7B9B5A0ECB35DCEC31B8A8122C37D687369253B5;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// InterfaceAndShareCreation
struct InterfaceAndShareCreation_t5513BC7BED3D6CFA175D244B711E9ED8D23574EB;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// NativeShare
struct NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B;
// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// Assets.Scripts.PopupShare
struct PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670;
// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// ScriptOfRotation
struct ScriptOfRotation_tCE7FA6957CB90E333D974DA464C250F0B3C44F30;
// ShareSocialMedia
struct ShareSocialMedia_t1C0A4433B74E5247C06740C2D347E17B41103318;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62;
// UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// UnityEngine.Events.UnityAction
struct UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F;
// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6;
// UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A;
// System.Uri
struct Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663;
// Assets.Scripts.CRMRequest/<ExecuteRequest>d__5
struct U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C;
// InterfaceAndShareCreation/<takeScreenShotAndShare>d__4
struct U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8;
// NativeShare/ShareResultCallback
struct ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5;
// Assets.Scripts.PopupShare/<>c
struct U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0;
// Assets.Scripts.PopupShare/<SendProspect>d__5
struct U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121;
// Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9
struct U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045;

IL2CPP_EXTERN_C RuntimeClass* CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Result_tFB98154F15BF37A66902802D441FEFADC68D4C87_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ShareResult_t2D0F0DD75A0DAD3506F82AC32C648A14B220EBFB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral131E50794C620609B15FB7C74D356CFC437817F9;
IL2CPP_EXTERN_C String_t* _stringLiteral14E338D17C42E552FA7AF42CDAE40CA1F0E8A04D;
IL2CPP_EXTERN_C String_t* _stringLiteral1BBA6C460982CB1E17796461EC18D23B78499EC9;
IL2CPP_EXTERN_C String_t* _stringLiteral38D17E2601C941F78434AE3242235ABF38F2F04A;
IL2CPP_EXTERN_C String_t* _stringLiteral3C0A1543E383C2369C1F5A84A51123EE833996E6;
IL2CPP_EXTERN_C String_t* _stringLiteral3CC4EE2F1BD70C342A8E205E793507D2A5FBB278;
IL2CPP_EXTERN_C String_t* _stringLiteral51A0DD408B1E4A956F27BA2871BD8E31E2668D06;
IL2CPP_EXTERN_C String_t* _stringLiteral5B58EBE31E594BF8FA4BEA3CD075473149322B18;
IL2CPP_EXTERN_C String_t* _stringLiteral64058CC688A96A90239811EF06C9D20DB0499C3E;
IL2CPP_EXTERN_C String_t* _stringLiteral695F3476E0CF840C28A2CC832105DBB60D8E2F91;
IL2CPP_EXTERN_C String_t* _stringLiteral7055A3A705CAD150F81C21ABD4C8B99578A13480;
IL2CPP_EXTERN_C String_t* _stringLiteral7AE60451A2BCF21BEE4D2DC2950F17ADAB860FBF;
IL2CPP_EXTERN_C String_t* _stringLiteral8C97CD74145CAD87BA4F6B7655948B0FC6CF8849;
IL2CPP_EXTERN_C String_t* _stringLiteral97503ABF7FACF2EB6EC76468DEFB6B64D1C5A38F;
IL2CPP_EXTERN_C String_t* _stringLiteral9D5A3AE3D2B0B5E5AF5AB489000D9B88FA11E907;
IL2CPP_EXTERN_C String_t* _stringLiteralA71EDAED3CCB56D2E41C3707602EBFDEFF3DF60B;
IL2CPP_EXTERN_C String_t* _stringLiteralAA473D3C6DEC47486536A8DCA1C955417EAEDF41;
IL2CPP_EXTERN_C String_t* _stringLiteralC8C7D64BAE2BD1D326A65A27033A07E3AC8CA97C;
IL2CPP_EXTERN_C String_t* _stringLiteralCB275B81030802F8A641A30B4F8E658C1433C8A0;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralFB8D59685D0F2F9A261BF3E5B8C621FFBD0A0D78;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mC898F7E3D9541F17BD8B79579FDD431C0651E12D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CExecuteRequestU3Ed__5_System_Collections_IEnumerator_Reset_m7B0FA514DC8A14D22DF202136F36FD06D715FFDD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CSendProspectU3Ed__5_System_Collections_IEnumerator_Reset_m7AC1900C505A46F9E0BF9AA3868A136401C50D97_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTakeScreenShotAndShareU3Ed__9_System_Collections_IEnumerator_Reset_mDAD2C4081C586643306F12C63330087F76F534B7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CTakeScreenShotAndShareU3Eb__9_0_m8F0A62F22FB592BCF910DF68CC8B45C402A7F5DC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CtakeScreenShotAndShareU3Ed__4_System_Collections_IEnumerator_Reset_m1100C1B8AFCD31C33805C692A5EA3829B93ED312_RuntimeMethod_var;
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com;
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke;
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tF2005780856147C45868EA541E393C1306010309 
{
};
struct Il2CppArrayBounds;

// Assets.Scripts.CRMLink
struct CRMLink_t54EC67907837D04A6A426F71A41123CB7A8E5778  : public RuntimeObject
{
};

// Assets.Scripts.CRMRequest
struct CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B  : public RuntimeObject
{
	// System.String Assets.Scripts.CRMRequest::url
	String_t* ___url_0;
	// System.String Assets.Scripts.CRMRequest::data
	String_t* ___data_1;
	// System.String Assets.Scripts.CRMRequest::method
	String_t* ___method_2;
	// System.String Assets.Scripts.CRMRequest::result
	String_t* ___result_3;
};

// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095  : public RuntimeObject
{
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t52460FA30AE37F4F26ACB81055E58002262F19F2* ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_tD2C40CE114AA9D8E1F7196608B2D088548015293* ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t7324102215E4ED41EC065C02EB501CB0BC23CD90* ___decoderFallback_14;
};

struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095_StaticFields
{
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___latin1Encoding_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Text.Encoding> modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Dictionary_2_t87EDE08B2E48F793A22DE50D6B3CC2E7EBB2DB54* ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject* ___s_InternalSyncObject_15;
};

// NativeShare
struct NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B  : public RuntimeObject
{
	// System.String NativeShare::subject
	String_t* ___subject_2;
	// System.String NativeShare::text
	String_t* ___text_3;
	// System.String NativeShare::title
	String_t* ___title_4;
	// System.String NativeShare::url
	String_t* ___url_5;
	// System.Collections.Generic.List`1<System.String> NativeShare::emailRecipients
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___emailRecipients_6;
	// System.Collections.Generic.List`1<System.String> NativeShare::targetPackages
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___targetPackages_7;
	// System.Collections.Generic.List`1<System.String> NativeShare::targetClasses
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___targetClasses_8;
	// System.Collections.Generic.List`1<System.String> NativeShare::files
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___files_9;
	// System.Collections.Generic.List`1<System.String> NativeShare::mimes
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___mimes_10;
	// NativeShare/ShareResultCallback NativeShare::callback
	ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5* ___callback_11;
};

struct NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B_StaticFields
{
	// UnityEngine.AndroidJavaClass NativeShare::m_ajc
	AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03* ___m_ajc_0;
	// UnityEngine.AndroidJavaObject NativeShare::m_context
	AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* ___m_context_1;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// Assets.Scripts.CRMRequest/<ExecuteRequest>d__5
struct U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C  : public RuntimeObject
{
	// System.Int32 Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// Assets.Scripts.CRMRequest Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::<>4__this
	CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::<request>5__1
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CrequestU3E5__1_3;
	// System.Byte[] Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::<bodyRaw>5__2
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___U3CbodyRawU3E5__2_4;
};

// InterfaceAndShareCreation/<takeScreenShotAndShare>d__4
struct U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E  : public RuntimeObject
{
	// System.Int32 InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// InterfaceAndShareCreation InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::<>4__this
	InterfaceAndShareCreation_t5513BC7BED3D6CFA175D244B711E9ED8D23574EB* ___U3CU3E4__this_2;
	// UnityEngine.Texture2D InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::<tx>5__1
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___U3CtxU3E5__1_3;
	// System.String InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::<path>5__2
	String_t* ___U3CpathU3E5__2_4;
	// UnityEngine.GameObject InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::<popupCanvasInstance>5__3
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___U3CpopupCanvasInstanceU3E5__3_5;
};

// Assets.Scripts.PopupShare/<>c
struct U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0  : public RuntimeObject
{
};

struct U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_StaticFields
{
	// Assets.Scripts.PopupShare/<>c Assets.Scripts.PopupShare/<>c::<>9
	U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0* ___U3CU3E9_0;
	// NativeShare/ShareResultCallback Assets.Scripts.PopupShare/<>c::<>9__9_0
	ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5* ___U3CU3E9__9_0_1;
};

// Assets.Scripts.PopupShare/<SendProspect>d__5
struct U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121  : public RuntimeObject
{
	// System.Int32 Assets.Scripts.PopupShare/<SendProspect>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Assets.Scripts.PopupShare/<SendProspect>d__5::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// Assets.Scripts.PopupShare Assets.Scripts.PopupShare/<SendProspect>d__5::<>4__this
	PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* ___U3CU3E4__this_2;
	// System.String Assets.Scripts.PopupShare/<SendProspect>d__5::<email>5__1
	String_t* ___U3CemailU3E5__1_3;
	// System.String Assets.Scripts.PopupShare/<SendProspect>d__5::<name>5__2
	String_t* ___U3CnameU3E5__2_4;
	// Assets.Scripts.CRMRequest Assets.Scripts.PopupShare/<SendProspect>d__5::<request>5__3
	CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* ___U3CrequestU3E5__3_5;
};

// Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9
struct U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045  : public RuntimeObject
{
	// System.Int32 Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// Assets.Scripts.PopupShare Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::<>4__this
	PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* ___U3CU3E4__this_2;
	// System.String Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::<path>5__1
	String_t* ___U3CpathU3E5__1_3;
};

// Unity.Collections.NativeArray`1<System.Byte>
struct NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF 
{
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// UnityEngine.Rect
struct Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D 
{
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
};

// UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB* ___m_completeCallback_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};

// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.CertificateHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974  : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB
{
	// Unity.Collections.NativeArray`1<System.Byte> UnityEngine.Networking.DownloadHandlerBuffer::m_NativeData
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_marshaled_pinvoke : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_marshaled_com : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700_StaticFields
{
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;
};

// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::m_DownloadHandler
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* ___m_DownloadHandler_1;
	// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::m_UploadHandler
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6* ___m_UploadHandler_2;
	// UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::m_CertificateHandler
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804* ___m_CertificateHandler_3;
	// System.Uri UnityEngine.Networking.UnityWebRequest::m_Uri
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeCertificateHandlerOnDispose>k__BackingField
	bool ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke ___m_DownloadHandler_1;
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke ___m_UploadHandler_2;
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_pinvoke ___m_CertificateHandler_3;
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com* ___m_DownloadHandler_1;
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com* ___m_UploadHandler_2;
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com* ___m_CertificateHandler_3;
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};

// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C  : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C
{
	// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::<webRequest>k__BackingField
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C_marshaled_pinvoke : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke
{
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C_marshaled_com : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com
{
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com* ___U3CwebRequestU3Ek__BackingField_2;
};

// UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A  : public UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6
{
	// Unity.Collections.NativeArray`1<System.Byte> UnityEngine.Networking.UploadHandlerRaw::m_Payload
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_Payload_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A_marshaled_pinvoke : public UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_Payload_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A_marshaled_com : public UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_Payload_1;
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// NativeShare/ShareResultCallback
struct ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5  : public MulticastDelegate_t
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// InterfaceAndShareCreation
struct InterfaceAndShareCreation_t5513BC7BED3D6CFA175D244B711E9ED8D23574EB  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String InterfaceAndShareCreation::photoName
	String_t* ___photoName_4;
	// System.String InterfaceAndShareCreation::popupName
	String_t* ___popupName_5;
	// UnityEngine.GameObject InterfaceAndShareCreation::popupCanvasPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___popupCanvasPrefab_6;
};

// Assets.Scripts.PopupShare
struct PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String Assets.Scripts.PopupShare::photoName
	String_t* ___photoName_4;
	// System.String Assets.Scripts.PopupShare::hashTags
	String_t* ___hashTags_5;
	// System.String Assets.Scripts.PopupShare::chooserText
	String_t* ___chooserText_6;
};

// ScriptOfRotation
struct ScriptOfRotation_tCE7FA6957CB90E333D974DA464C250F0B3C44F30  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single ScriptOfRotation::rotateSpeed
	float ___rotateSpeed_4;
};

// ShareSocialMedia
struct ShareSocialMedia_t1C0A4433B74E5247C06740C2D347E17B41103318  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860* ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4* ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;
};

struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE* ___s_VertexHelper_21;
};

// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E  : public Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931
{
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670* ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8* ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___m_Corners_35;
};

// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224* ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F* ___m_TempVerts_42;
};

struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultText_40;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// T UnityEngine.Object::Instantiate<System.Object>(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Object_Instantiate_TisRuntimeObject_mCD6FC6BB14BA9EF1A4B314841EB4D40675E3C1DB_gshared (RuntimeObject* ___original0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;

// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51 (String_t* ___name0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___exists0, const RuntimeMethod* method) ;
// System.Collections.IEnumerator InterfaceAndShareCreation::takeScreenShotAndShare()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InterfaceAndShareCreation_takeScreenShotAndShare_m06DE12C46F0CE2D4E2AE5FFE5A03EE7DDA4D9B91 (InterfaceAndShareCreation_t5513BC7BED3D6CFA175D244B711E9ED8D23574EB* __this, const RuntimeMethod* method) ;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812 (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, RuntimeObject* ___routine0, const RuntimeMethod* method) ;
// System.Void InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CtakeScreenShotAndShareU3Ed__4__ctor_m9D9AA0A1FB3C7E0B3AB1C1371024C672B44C7F3C (U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_m4AF7E576C01E6B04443BB898B1AE5D645F7D45AB (WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C (const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8 (const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_mECF60A9EC0638EC353C02C8E99B6B465D23BE917 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23 (Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D* __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_ReadPixels_m6B45DF7C051BF599C72ED09691F21A6C769EEBD9 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___source0, int32_t ___destX1, int32_t ___destY2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::Apply()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_mA014182C9EE0BBF6EEE3B286854F29E50EB972DC (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, const RuntimeMethod* method) ;
// System.String UnityEngine.Application::get_temporaryCachePath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Application_get_temporaryCachePath_m1C071883726EAFA66324E50FE73A1145272A4C60 (const RuntimeMethod* method) ;
// System.String System.IO.Path::Combine(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_Combine_m64754D4E08990CE1EBC41CDF197807EE4B115474 (String_t* ___path10, String_t* ___path21, const RuntimeMethod* method) ;
// System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ImageConversion_EncodeToPNG_m0FFFD0F0DC0EC22073BC937A5294067C57008391 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___tex0, const RuntimeMethod* method) ;
// System.Void System.IO.File::WriteAllBytes(System.String,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_WriteAllBytes_m72C1A24339B30F84A655E6BF516AE1638B2C4668 (String_t* ___path0, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___bytes1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, const RuntimeMethod* method) ;
// T UnityEngine.Object::Instantiate<UnityEngine.GameObject>(T)
inline GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mC898F7E3D9541F17BD8B79579FDD431C0651E12D (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original0, const RuntimeMethod* method)
{
	return ((  GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mCD6FC6BB14BA9EF1A4B314841EB4D40675E3C1DB_gshared)(___original0, method);
}
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* __this, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D (const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m7EA47AD57F43D478CCB0523D179950EE49CDA3E2 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, float ___xAngle0, float ___yAngle1, float ___zAngle2, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219 (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0 (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___values0, const RuntimeMethod* method) ;
// System.Void Assets.Scripts.CRMRequest::.ctor(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CRMRequest__ctor_mAFE2111C4BC7CBCAFAE5F5287D3184C2D97A5511 (CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* __this, String_t* ___url0, String_t* ___data1, String_t* ___method2, const RuntimeMethod* method) ;
// System.Void Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CExecuteRequestU3Ed__5__ctor_m1FC9C91CFF1F90B800ABA80927A69673776D85D9 (U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CExecuteRequestU3Ed__5_U3CU3Em__Finally1_mB9CCA43D3A8189903DBB1523E5D6960DAD73CA06 (U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C* __this, const RuntimeMethod* method) ;
// System.Void Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CExecuteRequestU3Ed__5_System_IDisposable_Dispose_mC2D7D9CB43A2119FF53D1D201714AED5DE6343EA (U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityWebRequest__ctor_mD4739B0BF1C4937479A83B24B531C6B819712A3E (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, String_t* ___url0, String_t* ___method1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Networking.UnityWebRequest::SetRequestHeader(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityWebRequest_SetRequestHeader_m099734EB787B7269B62AB2236A5A4F7D35AF8BC5 (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, String_t* ___name0, String_t* ___value1, const RuntimeMethod* method) ;
// System.Text.Encoding System.Text.Encoding::get_UTF8()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* Encoding_get_UTF8_m9700ADA8E0F244002B2A89B483F1B2133B8FE336 (const RuntimeMethod* method) ;
// System.Void UnityEngine.Networking.UploadHandlerRaw::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UploadHandlerRaw__ctor_m168C957B67E29CB3072E3542044D37E2F16C42B7 (UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Networking.UploadHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityWebRequest_set_uploadHandler_m68F346550136DE178C79238944985892196027FE (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Networking.DownloadHandlerBuffer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DownloadHandlerBuffer__ctor_m5EE7C9E8AB468B2B937A7C9C66B4176A884147AF (DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Networking.DownloadHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityWebRequest_set_downloadHandler_m6CB94492012097DFC44E5773D1A627753788292F (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* ___value0, const RuntimeMethod* method) ;
// UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::SendWebRequest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C* UnityWebRequest_SendWebRequest_mA3CD13983BAA5074A0640EDD661B1E46E6DB6C13 (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, const RuntimeMethod* method) ;
// UnityEngine.Networking.UnityWebRequest/Result UnityEngine.Networking.UnityWebRequest::get_result()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityWebRequest_get_result_mEF83848C5FCFB5E307CE4B57E42BF02FC9AED449 (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* UnityWebRequest_get_downloadHandler_m1AA91B23D9D594A4F4FE2975FC356C508528F1D5 (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, const RuntimeMethod* method) ;
// System.String UnityEngine.Networking.DownloadHandler::get_text()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* DownloadHandler_get_text_mA6DE5CB2647A21E577B963708DC3D0DA4DBFE7D8 (DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* __this, const RuntimeMethod* method) ;
// System.Int64 UnityEngine.Networking.UnityWebRequest::get_responseCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t UnityWebRequest_get_responseCode_m012C177F61435D5D120A21D7A03FFF7B0F8B904B (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, const RuntimeMethod* method) ;
// System.Collections.IEnumerator Assets.Scripts.PopupShare::SendProspect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PopupShare_SendProspect_m003EBA708009483799FF2A3A59102FB9CC2C9355 (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void Assets.Scripts.PopupShare/<SendProspect>d__5::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSendProspectU3Ed__5__ctor_m8C002DE64B385DD045C0A29C316B07D6627D1750 (U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
inline Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Collections.IEnumerator Assets.Scripts.PopupShare::TakeScreenShotAndShare()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PopupShare_TakeScreenShotAndShare_mCF21B51D0B99F6BF90664078315A12FE483CBC24 (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) ;
// System.Void Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTakeScreenShotAndShareU3Ed__9__ctor_m070ADBD890C72EFFED92984E3B99570B2EDDCEBB (U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.String Assets.Scripts.PopupShare::RetrieveDataEmail()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PopupShare_RetrieveDataEmail_mB102881EBEB87E60C26DED09BEBD9901F9802202 (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) ;
// System.String Assets.Scripts.PopupShare::RetrieveDataName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PopupShare_RetrieveDataName_m868A033D28CA3F41309605DD6AA9D45AC91BE295 (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) ;
// Assets.Scripts.CRMRequest Assets.Scripts.CRMLink::RetrieveData(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* CRMLink_RetrieveData_m73A09E347AFFC05FD0775C95997954A9F7010A40 (String_t* ___emailToSend0, String_t* ___nameToSend1, const RuntimeMethod* method) ;
// System.Collections.IEnumerator Assets.Scripts.CRMRequest::ExecuteRequest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CRMRequest_ExecuteRequest_mB68851B83798BA37E6FB4E681CC53EAFDA25C243 (CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* __this, const RuntimeMethod* method) ;
// System.Void Assets.Scripts.PopupShare::ShareScore()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PopupShare_ShareScore_m3671335DF1600A888B819F5A3B12D66445F184FC (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) ;
// System.Void Assets.Scripts.PopupShare/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m0543D9E3F5983200FBDAEDA716EBDEA881C496DC (U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0* __this, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806 (String_t* ___format0, RuntimeObject* ___arg01, RuntimeObject* ___arg12, const RuntimeMethod* method) ;
// System.Void NativeShare::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeShare__ctor_m7808C679AB06B9BE0BFF3FA5FF0F7FD06C51BE6E (NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* __this, const RuntimeMethod* method) ;
// NativeShare NativeShare::AddFile(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* NativeShare_AddFile_m2B6FF1B19C51E3229002A8430D253C1EFC0DD240 (NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* __this, String_t* ___filePath0, String_t* ___mime1, const RuntimeMethod* method) ;
// NativeShare NativeShare::SetSubject(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* NativeShare_SetSubject_mB0E01F5228D5C868274067653497BFD0F1EF1D8A (NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* __this, String_t* ___subject0, const RuntimeMethod* method) ;
// NativeShare NativeShare::SetText(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* NativeShare_SetText_mEB89C82F62181F7754C20F3516754B81559DBA13 (NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* __this, String_t* ___text0, const RuntimeMethod* method) ;
// NativeShare NativeShare::SetUrl(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* NativeShare_SetUrl_m5A85D24E53BA393226DD13089A85DC9044B294A6 (NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* __this, String_t* ___url0, const RuntimeMethod* method) ;
// System.Void NativeShare/ShareResultCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShareResultCallback__ctor_m916C36D840FC04394DB3549444BFA1E4C79F8AD4 (ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// NativeShare NativeShare::SetCallback(NativeShare/ShareResultCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* NativeShare_SetCallback_m546C0EEF61664E6B0B4A0318044A16BBBA0B5179 (NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* __this, ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5* ___callback0, const RuntimeMethod* method) ;
// System.Void NativeShare::Share()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeShare_Share_m3DB5879D819C16FC4325D124E296F6C6CD9C13DD (NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* __this, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InterfaceAndShareCreation::onClickShare()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InterfaceAndShareCreation_onClickShare_mE7D824C00FAE07677885A55E70B0959DF9659E7D (InterfaceAndShareCreation_t5513BC7BED3D6CFA175D244B711E9ED8D23574EB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// if (!GameObject.Find(popupName))
		String_t* L_0 = __this->___popupName_5;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(L_0, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_1, NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// StartCoroutine(takeScreenShotAndShare());
		RuntimeObject* L_4;
		L_4 = InterfaceAndShareCreation_takeScreenShotAndShare_m06DE12C46F0CE2D4E2AE5FFE5A03EE7DDA4D9B91(__this, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_5;
		L_5 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_4, NULL);
	}

IL_0027:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator InterfaceAndShareCreation::takeScreenShotAndShare()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InterfaceAndShareCreation_takeScreenShotAndShare_m06DE12C46F0CE2D4E2AE5FFE5A03EE7DDA4D9B91 (InterfaceAndShareCreation_t5513BC7BED3D6CFA175D244B711E9ED8D23574EB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E* L_0 = (U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E*)il2cpp_codegen_object_new(U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CtakeScreenShotAndShareU3Ed__4__ctor_m9D9AA0A1FB3C7E0B3AB1C1371024C672B44C7F3C(L_0, 0, NULL);
		U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void InterfaceAndShareCreation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InterfaceAndShareCreation__ctor_m12318221668FA3A5A9EBA56A5E8D993C90392114 (InterfaceAndShareCreation_t5513BC7BED3D6CFA175D244B711E9ED8D23574EB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1BBA6C460982CB1E17796461EC18D23B78499EC9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8C97CD74145CAD87BA4F6B7655948B0FC6CF8849);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string photoName = "temp_screenshot";
		__this->___photoName_4 = _stringLiteral8C97CD74145CAD87BA4F6B7655948B0FC6CF8849;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___photoName_4), (void*)_stringLiteral8C97CD74145CAD87BA4F6B7655948B0FC6CF8849);
		// public string popupName = "PopupSharing";
		__this->___popupName_5 = _stringLiteral1BBA6C460982CB1E17796461EC18D23B78499EC9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___popupName_5), (void*)_stringLiteral1BBA6C460982CB1E17796461EC18D23B78499EC9);
		// public GameObject popupCanvasPrefab = null;
		__this->___popupCanvasPrefab_6 = (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___popupCanvasPrefab_6), (void*)(GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)NULL);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CtakeScreenShotAndShareU3Ed__4__ctor_m9D9AA0A1FB3C7E0B3AB1C1371024C672B44C7F3C (U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CtakeScreenShotAndShareU3Ed__4_System_IDisposable_Dispose_m27440EA5327B75ED8170A14A34B0B0CF166BE91E (U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CtakeScreenShotAndShareU3Ed__4_MoveNext_mEDF9CB39447555EE99D8B1B1815D45C0B0317363 (U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mC898F7E3D9541F17BD8B79579FDD431C0651E12D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1BBA6C460982CB1E17796461EC18D23B78499EC9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3CC4EE2F1BD70C342A8E205E793507D2A5FBB278);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_0034;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* L_3 = (WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663*)il2cpp_codegen_object_new(WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		WaitForEndOfFrame__ctor_m4AF7E576C01E6B04443BB898B1AE5D645F7D45AB(L_3, NULL);
		__this->___U3CU3E2__current_1 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_3);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0034:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// Texture2D tx = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
		int32_t L_4;
		L_4 = Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C(NULL);
		int32_t L_5;
		L_5 = Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8(NULL);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_6 = (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4*)il2cpp_codegen_object_new(Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		Texture2D__ctor_mECF60A9EC0638EC353C02C8E99B6B465D23BE917(L_6, L_4, L_5, 3, (bool)0, NULL);
		__this->___U3CtxU3E5__1_3 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CtxU3E5__1_3), (void*)L_6);
		// tx.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_7 = __this->___U3CtxU3E5__1_3;
		int32_t L_8;
		L_8 = Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C(NULL);
		int32_t L_9;
		L_9 = Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8(NULL);
		Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D L_10;
		memset((&L_10), 0, sizeof(L_10));
		Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23((&L_10), (0.0f), (0.0f), ((float)L_8), ((float)L_9), /*hidden argument*/NULL);
		NullCheck(L_7);
		Texture2D_ReadPixels_m6B45DF7C051BF599C72ED09691F21A6C769EEBD9(L_7, L_10, 0, 0, NULL);
		// tx.Apply();
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_11 = __this->___U3CtxU3E5__1_3;
		NullCheck(L_11);
		Texture2D_Apply_mA014182C9EE0BBF6EEE3B286854F29E50EB972DC(L_11, NULL);
		// string path = Path.Combine(Application.temporaryCachePath, "sharedImage.png");//image name
		String_t* L_12;
		L_12 = Application_get_temporaryCachePath_m1C071883726EAFA66324E50FE73A1145272A4C60(NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_13;
		L_13 = Path_Combine_m64754D4E08990CE1EBC41CDF197807EE4B115474(L_12, _stringLiteral3CC4EE2F1BD70C342A8E205E793507D2A5FBB278, NULL);
		__this->___U3CpathU3E5__2_4 = L_13;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CpathU3E5__2_4), (void*)L_13);
		// File.WriteAllBytes(path, tx.EncodeToPNG());
		String_t* L_14 = __this->___U3CpathU3E5__2_4;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_15 = __this->___U3CtxU3E5__1_3;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_16;
		L_16 = ImageConversion_EncodeToPNG_m0FFFD0F0DC0EC22073BC937A5294067C57008391(L_15, NULL);
		File_WriteAllBytes_m72C1A24339B30F84A655E6BF516AE1638B2C4668(L_14, L_16, NULL);
		// Destroy(tx); //to avoid memory leaks
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_17 = __this->___U3CtxU3E5__1_3;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_17, NULL);
		// GameObject popupCanvasInstance = Instantiate(popupCanvasPrefab);
		InterfaceAndShareCreation_t5513BC7BED3D6CFA175D244B711E9ED8D23574EB* L_18 = __this->___U3CU3E4__this_2;
		NullCheck(L_18);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_19 = L_18->___popupCanvasPrefab_6;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_20;
		L_20 = Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mC898F7E3D9541F17BD8B79579FDD431C0651E12D(L_19, Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_mC898F7E3D9541F17BD8B79579FDD431C0651E12D_RuntimeMethod_var);
		__this->___U3CpopupCanvasInstanceU3E5__3_5 = L_20;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CpopupCanvasInstanceU3E5__3_5), (void*)L_20);
		// popupCanvasInstance.name = "PopupSharing";
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_21 = __this->___U3CpopupCanvasInstanceU3E5__3_5;
		NullCheck(L_21);
		Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47(L_21, _stringLiteral1BBA6C460982CB1E17796461EC18D23B78499EC9, NULL);
		// }
		return (bool)0;
	}
}
// System.Object InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CtakeScreenShotAndShareU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25B4A0ADEB37E2653490C9237962CB3BF8149C99 (U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CtakeScreenShotAndShareU3Ed__4_System_Collections_IEnumerator_Reset_m1100C1B8AFCD31C33805C692A5EA3829B93ED312 (U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CtakeScreenShotAndShareU3Ed__4_System_Collections_IEnumerator_Reset_m1100C1B8AFCD31C33805C692A5EA3829B93ED312_RuntimeMethod_var)));
	}
}
// System.Object InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CtakeScreenShotAndShareU3Ed__4_System_Collections_IEnumerator_get_Current_mFB72C197B1482C31171FF54C68FC859FC970DCAA (U3CtakeScreenShotAndShareU3Ed__4_tEC9BEDB9DA8F4FC217DFC0D7D954D8748D74BB2E* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ScriptOfRotation::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptOfRotation_Start_m69D4C4328177E6762A32E8A7B9C0CE545E888827 (ScriptOfRotation_tCE7FA6957CB90E333D974DA464C250F0B3C44F30* __this, const RuntimeMethod* method) 
{
	{
		// void Start() { }
		return;
	}
}
// System.Void ScriptOfRotation::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptOfRotation_Update_m81CEFC54FC3B8318B28D4EBB20BF43B43D27F750 (ScriptOfRotation_tCE7FA6957CB90E333D974DA464C250F0B3C44F30* __this, const RuntimeMethod* method) 
{
	{
		// transform.Rotate(0, rotateSpeed * Time.deltaTime, 0);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		float L_1 = __this->___rotateSpeed_4;
		float L_2;
		L_2 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		NullCheck(L_0);
		Transform_Rotate_m7EA47AD57F43D478CCB0523D179950EE49CDA3E2(L_0, (0.0f), ((float)il2cpp_codegen_multiply(L_1, L_2)), (0.0f), NULL);
		// }
		return;
	}
}
// System.Void ScriptOfRotation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptOfRotation__ctor_mCCE0AF7D2080009AC3FDA14ADDD9FABC17070D90 (ScriptOfRotation_tCE7FA6957CB90E333D974DA464C250F0B3C44F30* __this, const RuntimeMethod* method) 
{
	{
		// public float rotateSpeed = 20;
		__this->___rotateSpeed_4 = (20.0f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShareSocialMedia::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShareSocialMedia__ctor_m33388428688706DC50F42F1AB64B69A74DD273C0 (ShareSocialMedia_t1C0A4433B74E5247C06740C2D347E17B41103318* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Assets.Scripts.CRMRequest Assets.Scripts.CRMLink::RetrieveData(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* CRMLink_RetrieveData_m73A09E347AFFC05FD0775C95997954A9F7010A40 (String_t* ___emailToSend0, String_t* ___nameToSend1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral131E50794C620609B15FB7C74D356CFC437817F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14E338D17C42E552FA7AF42CDAE40CA1F0E8A04D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral38D17E2601C941F78434AE3242235ABF38F2F04A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C0A1543E383C2369C1F5A84A51123EE833996E6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral695F3476E0CF840C28A2CC832105DBB60D8E2F91);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8C7D64BAE2BD1D326A65A27033A07E3AC8CA97C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCB275B81030802F8A641A30B4F8E658C1433C8A0);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* V_2 = NULL;
	{
		// Debug.Log("email:" + emailToSend);
		String_t* L_0 = ___emailToSend0;
		String_t* L_1;
		L_1 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteralC8C7D64BAE2BD1D326A65A27033A07E3AC8CA97C, L_0, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_1, NULL);
		// Debug.Log("name:" + nameToSend);
		String_t* L_2 = ___nameToSend1;
		String_t* L_3;
		L_3 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteral131E50794C620609B15FB7C74D356CFC437817F9, L_2, NULL);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_3, NULL);
		// string url = "https://api.hsforms.com/submissions/v3/integration/submit/25923445/4e078095-3f17-4a6a-b26f-938676b23215";
		V_0 = _stringLiteral3C0A1543E383C2369C1F5A84A51123EE833996E6;
		// string data = "{\"fields\":[{\"objectTypeId\": \"0-1\",\"name\":\"email\",\"value\": \" "+emailToSend+" \" },{\"objectTypeId\": \"0-1\",\"name\":\"firstname\",\"value\": \" "+nameToSend+" \"  }]}";
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_4 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral38D17E2601C941F78434AE3242235ABF38F2F04A);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral38D17E2601C941F78434AE3242235ABF38F2F04A);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_6 = L_5;
		String_t* L_7 = ___emailToSend0;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_7);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_8 = L_6;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral695F3476E0CF840C28A2CC832105DBB60D8E2F91);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral695F3476E0CF840C28A2CC832105DBB60D8E2F91);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_9 = L_8;
		String_t* L_10 = ___nameToSend1;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_10);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = L_9;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteralCB275B81030802F8A641A30B4F8E658C1433C8A0);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralCB275B81030802F8A641A30B4F8E658C1433C8A0);
		String_t* L_12;
		L_12 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_11, NULL);
		V_1 = L_12;
		// Debug.Log(data);
		String_t* L_13 = V_1;
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_13, NULL);
		// return new CRMRequest(url, data, "POST");
		String_t* L_14 = V_0;
		String_t* L_15 = V_1;
		CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* L_16 = (CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B*)il2cpp_codegen_object_new(CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B_il2cpp_TypeInfo_var);
		NullCheck(L_16);
		CRMRequest__ctor_mAFE2111C4BC7CBCAFAE5F5287D3184C2D97A5511(L_16, L_14, L_15, _stringLiteral14E338D17C42E552FA7AF42CDAE40CA1F0E8A04D, NULL);
		V_2 = L_16;
		goto IL_006b;
	}

IL_006b:
	{
		// }
		CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* L_17 = V_2;
		return L_17;
	}
}
// System.Void Assets.Scripts.CRMLink::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CRMLink__ctor_m8D6FA061FD706D49EB100120D2B6DC8581C1EB12 (CRMLink_t54EC67907837D04A6A426F71A41123CB7A8E5778* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Assets.Scripts.CRMRequest::.ctor(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CRMRequest__ctor_mAFE2111C4BC7CBCAFAE5F5287D3184C2D97A5511 (CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* __this, String_t* ___url0, String_t* ___data1, String_t* ___method2, const RuntimeMethod* method) 
{
	{
		// public CRMRequest(string url, string data, string method)
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		// this.url = url;
		String_t* L_0 = ___url0;
		__this->___url_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___url_0), (void*)L_0);
		// this.data = data;
		String_t* L_1 = ___data1;
		__this->___data_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_1), (void*)L_1);
		// this.method = method;
		String_t* L_2 = ___method2;
		__this->___method_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___method_2), (void*)L_2);
		// }
		return;
	}
}
// System.Collections.IEnumerator Assets.Scripts.CRMRequest::ExecuteRequest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CRMRequest_ExecuteRequest_mB68851B83798BA37E6FB4E681CC53EAFDA25C243 (CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C* L_0 = (U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C*)il2cpp_codegen_object_new(U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CExecuteRequestU3Ed__5__ctor_m1FC9C91CFF1F90B800ABA80927A69673776D85D9(L_0, 0, NULL);
		U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CExecuteRequestU3Ed__5__ctor_m1FC9C91CFF1F90B800ABA80927A69673776D85D9 (U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CExecuteRequestU3Ed__5_System_IDisposable_Dispose_mC2D7D9CB43A2119FF53D1D201714AED5DE6343EA (U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0020;
	}

IL_0014:
	{
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0017:
			{// begin finally (depth: 1)
				U3CExecuteRequestU3Ed__5_U3CU3Em__Finally1_mB9CCA43D3A8189903DBB1523E5D6960DAD73CA06(__this, NULL);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			goto IL_001e;
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_001e:
	{
		goto IL_0020;
	}

IL_0020:
	{
		return;
	}
}
// System.Boolean Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CExecuteRequestU3Ed__5_MoveNext_m6D86FA1D5EA48ACEEE511E27EC7F9A7125A7B283 (U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Result_tFB98154F15BF37A66902802D441FEFADC68D4C87_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5B58EBE31E594BF8FA4BEA3CD075473149322B18);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral64058CC688A96A90239811EF06C9D20DB0499C3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9D5A3AE3D2B0B5E5AF5AB489000D9B88FA11E907);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFB8D59685D0F2F9A261BF3E5B8C621FFBD0A0D78);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		auto __finallyBlock = il2cpp::utils::Fault([&]
		{

FAULT_0195:
			{// begin fault (depth: 1)
				U3CExecuteRequestU3Ed__5_System_IDisposable_Dispose_mC2D7D9CB43A2119FF53D1D201714AED5DE6343EA(__this, NULL);
				return;
			}// end fault
		});
		try
		{// begin try (depth: 1)
			{
				int32_t L_0 = __this->___U3CU3E1__state_0;
				V_1 = L_0;
				int32_t L_1 = V_1;
				if (!L_1)
				{
					goto IL_0012_1;
				}
			}
			{
				goto IL_000c_1;
			}

IL_000c_1:
			{
				int32_t L_2 = V_1;
				if ((((int32_t)L_2) == ((int32_t)1)))
				{
					goto IL_0014_1;
				}
			}
			{
				goto IL_0019_1;
			}

IL_0012_1:
			{
				goto IL_0020_1;
			}

IL_0014_1:
			{
				goto IL_00df_1;
			}

IL_0019_1:
			{
				V_0 = (bool)0;
				goto IL_019d;
			}

IL_0020_1:
			{
				__this->___U3CU3E1__state_0 = (-1);
				// using UnityWebRequest request = new UnityWebRequest(this.url, this.method);
				CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* L_3 = __this->___U3CU3E4__this_2;
				NullCheck(L_3);
				String_t* L_4 = L_3->___url_0;
				CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* L_5 = __this->___U3CU3E4__this_2;
				NullCheck(L_5);
				String_t* L_6 = L_5->___method_2;
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_7 = (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F*)il2cpp_codegen_object_new(UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_il2cpp_TypeInfo_var);
				NullCheck(L_7);
				UnityWebRequest__ctor_mD4739B0BF1C4937479A83B24B531C6B819712A3E(L_7, L_4, L_6, NULL);
				__this->___U3CrequestU3E5__1_3 = L_7;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CrequestU3E5__1_3), (void*)L_7);
				__this->___U3CU3E1__state_0 = ((int32_t)-3);
				// request.SetRequestHeader("Content-Type", "application/json");
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_8 = __this->___U3CrequestU3E5__1_3;
				NullCheck(L_8);
				UnityWebRequest_SetRequestHeader_m099734EB787B7269B62AB2236A5A4F7D35AF8BC5(L_8, _stringLiteral5B58EBE31E594BF8FA4BEA3CD075473149322B18, _stringLiteral64058CC688A96A90239811EF06C9D20DB0499C3E, NULL);
				// request.SetRequestHeader("Authorization", "Bearer pat-eu1-a18a3a99-b22b-410d-838b-b58f08bbcba0");
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_9 = __this->___U3CrequestU3E5__1_3;
				NullCheck(L_9);
				UnityWebRequest_SetRequestHeader_m099734EB787B7269B62AB2236A5A4F7D35AF8BC5(L_9, _stringLiteral9D5A3AE3D2B0B5E5AF5AB489000D9B88FA11E907, _stringLiteralFB8D59685D0F2F9A261BF3E5B8C621FFBD0A0D78, NULL);
				// byte[] bodyRaw = Encoding.UTF8.GetBytes(this.data);
				Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_10;
				L_10 = Encoding_get_UTF8_m9700ADA8E0F244002B2A89B483F1B2133B8FE336(NULL);
				CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* L_11 = __this->___U3CU3E4__this_2;
				NullCheck(L_11);
				String_t* L_12 = L_11->___data_1;
				NullCheck(L_10);
				ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_13;
				L_13 = VirtualFuncInvoker1< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, String_t* >::Invoke(17 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_10, L_12);
				__this->___U3CbodyRawU3E5__2_4 = L_13;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CbodyRawU3E5__2_4), (void*)L_13);
				// request.uploadHandler = new UploadHandlerRaw(bodyRaw);
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_14 = __this->___U3CrequestU3E5__1_3;
				ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_15 = __this->___U3CbodyRawU3E5__2_4;
				UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A* L_16 = (UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A*)il2cpp_codegen_object_new(UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A_il2cpp_TypeInfo_var);
				NullCheck(L_16);
				UploadHandlerRaw__ctor_m168C957B67E29CB3072E3542044D37E2F16C42B7(L_16, L_15, NULL);
				NullCheck(L_14);
				UnityWebRequest_set_uploadHandler_m68F346550136DE178C79238944985892196027FE(L_14, L_16, NULL);
				// request.downloadHandler = new DownloadHandlerBuffer();
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_17 = __this->___U3CrequestU3E5__1_3;
				DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974* L_18 = (DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974*)il2cpp_codegen_object_new(DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_il2cpp_TypeInfo_var);
				NullCheck(L_18);
				DownloadHandlerBuffer__ctor_m5EE7C9E8AB468B2B937A7C9C66B4176A884147AF(L_18, NULL);
				NullCheck(L_17);
				UnityWebRequest_set_downloadHandler_m6CB94492012097DFC44E5773D1A627753788292F(L_17, L_18, NULL);
				// yield return request.SendWebRequest();
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_19 = __this->___U3CrequestU3E5__1_3;
				NullCheck(L_19);
				UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C* L_20;
				L_20 = UnityWebRequest_SendWebRequest_mA3CD13983BAA5074A0640EDD661B1E46E6DB6C13(L_19, NULL);
				__this->___U3CU3E2__current_1 = L_20;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_20);
				__this->___U3CU3E1__state_0 = 1;
				V_0 = (bool)1;
				goto IL_019d;
			}

IL_00df_1:
			{
				__this->___U3CU3E1__state_0 = ((int32_t)-3);
				// if (request.result == UnityWebRequest.Result.Success)
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_21 = __this->___U3CrequestU3E5__1_3;
				NullCheck(L_21);
				int32_t L_22;
				L_22 = UnityWebRequest_get_result_mEF83848C5FCFB5E307CE4B57E42BF02FC9AED449(L_21, NULL);
				V_2 = (bool)((((int32_t)L_22) == ((int32_t)1))? 1 : 0);
				bool L_23 = V_2;
				if (!L_23)
				{
					goto IL_0144_1;
				}
			}
			{
				// Debug.LogError(request.result);
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_24 = __this->___U3CrequestU3E5__1_3;
				NullCheck(L_24);
				int32_t L_25;
				L_25 = UnityWebRequest_get_result_mEF83848C5FCFB5E307CE4B57E42BF02FC9AED449(L_24, NULL);
				int32_t L_26 = L_25;
				RuntimeObject* L_27 = Box(Result_tFB98154F15BF37A66902802D441FEFADC68D4C87_il2cpp_TypeInfo_var, &L_26);
				il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
				Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E(L_27, NULL);
				// this.result = request.downloadHandler.text;
				CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* L_28 = __this->___U3CU3E4__this_2;
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_29 = __this->___U3CrequestU3E5__1_3;
				NullCheck(L_29);
				DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* L_30;
				L_30 = UnityWebRequest_get_downloadHandler_m1AA91B23D9D594A4F4FE2975FC356C508528F1D5(L_29, NULL);
				NullCheck(L_30);
				String_t* L_31;
				L_31 = DownloadHandler_get_text_mA6DE5CB2647A21E577B963708DC3D0DA4DBFE7D8(L_30, NULL);
				NullCheck(L_28);
				L_28->___result_3 = L_31;
				Il2CppCodeGenWriteBarrier((void**)(&L_28->___result_3), (void*)L_31);
				// Debug.LogError(request.downloadHandler.text);
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_32 = __this->___U3CrequestU3E5__1_3;
				NullCheck(L_32);
				DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* L_33;
				L_33 = UnityWebRequest_get_downloadHandler_m1AA91B23D9D594A4F4FE2975FC356C508528F1D5(L_32, NULL);
				NullCheck(L_33);
				String_t* L_34;
				L_34 = DownloadHandler_get_text_mA6DE5CB2647A21E577B963708DC3D0DA4DBFE7D8(L_33, NULL);
				Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E(L_34, NULL);
				goto IL_0188_1;
			}

IL_0144_1:
			{
				// Debug.LogError(request.result);
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_35 = __this->___U3CrequestU3E5__1_3;
				NullCheck(L_35);
				int32_t L_36;
				L_36 = UnityWebRequest_get_result_mEF83848C5FCFB5E307CE4B57E42BF02FC9AED449(L_35, NULL);
				int32_t L_37 = L_36;
				RuntimeObject* L_38 = Box(Result_tFB98154F15BF37A66902802D441FEFADC68D4C87_il2cpp_TypeInfo_var, &L_37);
				il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
				Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E(L_38, NULL);
				// Debug.LogError(request.responseCode);
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_39 = __this->___U3CrequestU3E5__1_3;
				NullCheck(L_39);
				int64_t L_40;
				L_40 = UnityWebRequest_get_responseCode_m012C177F61435D5D120A21D7A03FFF7B0F8B904B(L_39, NULL);
				int64_t L_41 = L_40;
				RuntimeObject* L_42 = Box(Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3_il2cpp_TypeInfo_var, &L_41);
				Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E(L_42, NULL);
				// Debug.LogError(request.downloadHandler.text);
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_43 = __this->___U3CrequestU3E5__1_3;
				NullCheck(L_43);
				DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* L_44;
				L_44 = UnityWebRequest_get_downloadHandler_m1AA91B23D9D594A4F4FE2975FC356C508528F1D5(L_43, NULL);
				NullCheck(L_44);
				String_t* L_45;
				L_45 = DownloadHandler_get_text_mA6DE5CB2647A21E577B963708DC3D0DA4DBFE7D8(L_44, NULL);
				Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E(L_45, NULL);
			}

IL_0188_1:
			{
				// }
				V_0 = (bool)0;
				goto IL_018c_1;
			}

IL_018c_1:
			{
				U3CExecuteRequestU3Ed__5_U3CU3Em__Finally1_mB9CCA43D3A8189903DBB1523E5D6960DAD73CA06(__this, NULL);
				goto IL_019d;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_019d:
	{
		bool L_46 = V_0;
		return L_46;
	}
}
// System.Void Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CExecuteRequestU3Ed__5_U3CU3Em__Finally1_mB9CCA43D3A8189903DBB1523E5D6960DAD73CA06 (U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->___U3CU3E1__state_0 = (-1);
		UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_0 = __this->___U3CrequestU3E5__1_3;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_1 = __this->___U3CrequestU3E5__1_3;
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_1);
	}

IL_001b:
	{
		return;
	}
}
// System.Object Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CExecuteRequestU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m399C5FABAD85503F0A3C2E0E93C7E8BFF71100D8 (U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CExecuteRequestU3Ed__5_System_Collections_IEnumerator_Reset_m7B0FA514DC8A14D22DF202136F36FD06D715FFDD (U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CExecuteRequestU3Ed__5_System_Collections_IEnumerator_Reset_m7B0FA514DC8A14D22DF202136F36FD06D715FFDD_RuntimeMethod_var)));
	}
}
// System.Object Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CExecuteRequestU3Ed__5_System_Collections_IEnumerator_get_Current_m789B018E9831759ECBEC342A042C76C1D5D16779 (U3CExecuteRequestU3Ed__5_t1C817F765453915680807B19030442440EE5039C* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Assets.Scripts.PopupShare::onClickSubmit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PopupShare_onClickSubmit_m47FDAB4CFEA0F40602145D23749EB9B1DA63FDF8 (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) 
{
	{
		// StartCoroutine(SendProspect());
		RuntimeObject* L_0;
		L_0 = PopupShare_SendProspect_m003EBA708009483799FF2A3A59102FB9CC2C9355(__this, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_1;
		L_1 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_0, NULL);
		// }
		return;
	}
}
// System.Void Assets.Scripts.PopupShare::onClickCancel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PopupShare_onClickCancel_m2BBEEB7D0FE62AEC47901F007FF2DE9701E998A9 (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Destroy(gameObject);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator Assets.Scripts.PopupShare::SendProspect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PopupShare_SendProspect_m003EBA708009483799FF2A3A59102FB9CC2C9355 (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121* L_0 = (U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121*)il2cpp_codegen_object_new(U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CSendProspectU3Ed__5__ctor_m8C002DE64B385DD045C0A29C316B07D6627D1750(L_0, 0, NULL);
		U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.String Assets.Scripts.PopupShare::RetrieveDataName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PopupShare_RetrieveDataName_m868A033D28CA3F41309605DD6AA9D45AC91BE295 (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral51A0DD408B1E4A956F27BA2871BD8E31E2668D06);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* V_0 = NULL;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		// GameObject t = GameObject.Find("nameText");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral51A0DD408B1E4A956F27BA2871BD8E31E2668D06, NULL);
		V_0 = L_0;
		// Text nameTextComponent = t.GetComponent<Text>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = V_0;
		NullCheck(L_1);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_2;
		L_2 = GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F(L_1, GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F_RuntimeMethod_var);
		V_1 = L_2;
		// string name = nameTextComponent.text;
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_3 = V_1;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_3);
		V_2 = L_4;
		// return name;
		String_t* L_5 = V_2;
		V_3 = L_5;
		goto IL_001e;
	}

IL_001e:
	{
		// }
		String_t* L_6 = V_3;
		return L_6;
	}
}
// System.String Assets.Scripts.PopupShare::RetrieveDataEmail()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PopupShare_RetrieveDataEmail_mB102881EBEB87E60C26DED09BEBD9901F9802202 (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA71EDAED3CCB56D2E41C3707602EBFDEFF3DF60B);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* V_0 = NULL;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		// GameObject t = GameObject.Find("emailText");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralA71EDAED3CCB56D2E41C3707602EBFDEFF3DF60B, NULL);
		V_0 = L_0;
		// Text nameTextComponent = t.GetComponent<Text>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = V_0;
		NullCheck(L_1);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_2;
		L_2 = GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F(L_1, GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F_RuntimeMethod_var);
		V_1 = L_2;
		// string email = nameTextComponent.text;
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_3 = V_1;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = VirtualFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_3);
		V_2 = L_4;
		// return email;
		String_t* L_5 = V_2;
		V_3 = L_5;
		goto IL_001e;
	}

IL_001e:
	{
		// }
		String_t* L_6 = V_3;
		return L_6;
	}
}
// System.Void Assets.Scripts.PopupShare::ShareScore()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PopupShare_ShareScore_m3671335DF1600A888B819F5A3B12D66445F184FC (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) 
{
	{
		// StartCoroutine(TakeScreenShotAndShare());
		RuntimeObject* L_0;
		L_0 = PopupShare_TakeScreenShotAndShare_mCF21B51D0B99F6BF90664078315A12FE483CBC24(__this, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_1;
		L_1 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator Assets.Scripts.PopupShare::TakeScreenShotAndShare()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PopupShare_TakeScreenShotAndShare_mCF21B51D0B99F6BF90664078315A12FE483CBC24 (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045* L_0 = (U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045*)il2cpp_codegen_object_new(U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CTakeScreenShotAndShareU3Ed__9__ctor_m070ADBD890C72EFFED92984E3B99570B2EDDCEBB(L_0, 0, NULL);
		U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void Assets.Scripts.PopupShare::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PopupShare__ctor_mBA5BCD2941AD1642A515FBC849745BFCFCA29409 (PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7055A3A705CAD150F81C21ABD4C8B99578A13480);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AE60451A2BCF21BEE4D2DC2950F17ADAB860FBF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8C97CD74145CAD87BA4F6B7655948B0FC6CF8849);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string photoName = "temp_screenshot";
		__this->___photoName_4 = _stringLiteral8C97CD74145CAD87BA4F6B7655948B0FC6CF8849;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___photoName_4), (void*)_stringLiteral8C97CD74145CAD87BA4F6B7655948B0FC6CF8849);
		// public string hashTags = "#cerealis #coloring #AR";
		__this->___hashTags_5 = _stringLiteral7055A3A705CAD150F81C21ABD4C8B99578A13480;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___hashTags_5), (void*)_stringLiteral7055A3A705CAD150F81C21ABD4C8B99578A13480);
		// public string chooserText = "Share with ...";
		__this->___chooserText_6 = _stringLiteral7AE60451A2BCF21BEE4D2DC2950F17ADAB860FBF;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___chooserText_6), (void*)_stringLiteral7AE60451A2BCF21BEE4D2DC2950F17ADAB860FBF);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Assets.Scripts.PopupShare/<SendProspect>d__5::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSendProspectU3Ed__5__ctor_m8C002DE64B385DD045C0A29C316B07D6627D1750 (U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void Assets.Scripts.PopupShare/<SendProspect>d__5::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSendProspectU3Ed__5_System_IDisposable_Dispose_mF560542FDB35C682B86C1BC34DFF98D422DDF311 (U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean Assets.Scripts.PopupShare/<SendProspect>d__5::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSendProspectU3Ed__5_MoveNext_m00BE9FB63626380C7DD6DBCB21D12230825518E5 (U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_0073;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// string email = RetrieveDataEmail();
		PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* L_3 = __this->___U3CU3E4__this_2;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = PopupShare_RetrieveDataEmail_mB102881EBEB87E60C26DED09BEBD9901F9802202(L_3, NULL);
		__this->___U3CemailU3E5__1_3 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CemailU3E5__1_3), (void*)L_4);
		// string name = RetrieveDataName();
		PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* L_5 = __this->___U3CU3E4__this_2;
		NullCheck(L_5);
		String_t* L_6;
		L_6 = PopupShare_RetrieveDataName_m868A033D28CA3F41309605DD6AA9D45AC91BE295(L_5, NULL);
		__this->___U3CnameU3E5__2_4 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CnameU3E5__2_4), (void*)L_6);
		// var request = CRMLink.RetrieveData(email, name);
		String_t* L_7 = __this->___U3CemailU3E5__1_3;
		String_t* L_8 = __this->___U3CnameU3E5__2_4;
		CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* L_9;
		L_9 = CRMLink_RetrieveData_m73A09E347AFFC05FD0775C95997954A9F7010A40(L_7, L_8, NULL);
		__this->___U3CrequestU3E5__3_5 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CrequestU3E5__3_5), (void*)L_9);
		// yield return request.ExecuteRequest();
		CRMRequest_t843F5122888FF7F7C911852FB75474C4D0C82F4B* L_10 = __this->___U3CrequestU3E5__3_5;
		NullCheck(L_10);
		RuntimeObject* L_11;
		L_11 = CRMRequest_ExecuteRequest_mB68851B83798BA37E6FB4E681CC53EAFDA25C243(L_10, NULL);
		__this->___U3CU3E2__current_1 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_11);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0073:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// ShareScore();
		PopupShare_t1B7CE2E31DAD71B6DD28626C1AF567810CCDD1FB* L_12 = __this->___U3CU3E4__this_2;
		NullCheck(L_12);
		PopupShare_ShareScore_m3671335DF1600A888B819F5A3B12D66445F184FC(L_12, NULL);
		// }
		return (bool)0;
	}
}
// System.Object Assets.Scripts.PopupShare/<SendProspect>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CSendProspectU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2E4E75AEBFEAEC93BB7612BF2FC67DE3953929A (U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void Assets.Scripts.PopupShare/<SendProspect>d__5::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSendProspectU3Ed__5_System_Collections_IEnumerator_Reset_m7AC1900C505A46F9E0BF9AA3868A136401C50D97 (U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CSendProspectU3Ed__5_System_Collections_IEnumerator_Reset_m7AC1900C505A46F9E0BF9AA3868A136401C50D97_RuntimeMethod_var)));
	}
}
// System.Object Assets.Scripts.PopupShare/<SendProspect>d__5::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CSendProspectU3Ed__5_System_Collections_IEnumerator_get_Current_m918BE55145B758EA758CEF4F871FDD670602A601 (U3CSendProspectU3Ed__5_tF2FD5792C6C568670FAE6D99C81A6F17A7810121* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Assets.Scripts.PopupShare/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m41313CCDEEE0F8FBF9A84FAA9981F81AE03A2B4F (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0* L_0 = (U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0*)il2cpp_codegen_object_new(U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__ctor_m0543D9E3F5983200FBDAEDA716EBDEA881C496DC(L_0, NULL);
		((U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_il2cpp_TypeInfo_var))->___U3CU3E9_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_il2cpp_TypeInfo_var))->___U3CU3E9_0), (void*)L_0);
		return;
	}
}
// System.Void Assets.Scripts.PopupShare/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m0543D9E3F5983200FBDAEDA716EBDEA881C496DC (U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void Assets.Scripts.PopupShare/<>c::<TakeScreenShotAndShare>b__9_0(NativeShare/ShareResult,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CTakeScreenShotAndShareU3Eb__9_0_m8F0A62F22FB592BCF910DF68CC8B45C402A7F5DC (U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0* __this, int32_t ___res0, String_t* ___target1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShareResult_t2D0F0DD75A0DAD3506F82AC32C648A14B220EBFB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97503ABF7FACF2EB6EC76468DEFB6B64D1C5A38F);
		s_Il2CppMethodInitialized = true;
	}
	{
		// .SetText("#cerealis #coloring #AR").SetUrl("").SetCallback((res, target) => Debug.LogError($"result {res}, target apps : {target}"))
		int32_t L_0 = ___res0;
		int32_t L_1 = L_0;
		RuntimeObject* L_2 = Box(ShareResult_t2D0F0DD75A0DAD3506F82AC32C648A14B220EBFB_il2cpp_TypeInfo_var, &L_1);
		String_t* L_3 = ___target1;
		String_t* L_4;
		L_4 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(_stringLiteral97503ABF7FACF2EB6EC76468DEFB6B64D1C5A38F, L_2, L_3, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E(L_4, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTakeScreenShotAndShareU3Ed__9__ctor_m070ADBD890C72EFFED92984E3B99570B2EDDCEBB (U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTakeScreenShotAndShareU3Ed__9_System_IDisposable_Dispose_mC9BA77A0F67E86FAB3B95737C42D1DC4EA122945 (U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTakeScreenShotAndShareU3Ed__9_MoveNext_mB2C48578AC3DF1F0E0D6A6BCB9663DF788694155 (U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CTakeScreenShotAndShareU3Eb__9_0_m8F0A62F22FB592BCF910DF68CC8B45C402A7F5DC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3CC4EE2F1BD70C342A8E205E793507D2A5FBB278);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7055A3A705CAD150F81C21ABD4C8B99578A13480);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAA473D3C6DEC47486536A8DCA1C955417EAEDF41);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5* G_B10_0 = NULL;
	NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* G_B10_1 = NULL;
	ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5* G_B9_0 = NULL;
	NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* G_B9_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_0034;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* L_3 = (WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663*)il2cpp_codegen_object_new(WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		WaitForEndOfFrame__ctor_m4AF7E576C01E6B04443BB898B1AE5D645F7D45AB(L_3, NULL);
		__this->___U3CU3E2__current_1 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_3);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0034:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// string path = Path.Combine(Application.temporaryCachePath, "sharedImage.png");
		String_t* L_4;
		L_4 = Application_get_temporaryCachePath_m1C071883726EAFA66324E50FE73A1145272A4C60(NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_5;
		L_5 = Path_Combine_m64754D4E08990CE1EBC41CDF197807EE4B115474(L_4, _stringLiteral3CC4EE2F1BD70C342A8E205E793507D2A5FBB278, NULL);
		__this->___U3CpathU3E5__1_3 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CpathU3E5__1_3), (void*)L_5);
		// new NativeShare()
		//     .AddFile(path)
		//     .SetSubject("I finish my draw !!!!")
		//     .SetText("#cerealis #coloring #AR").SetUrl("").SetCallback((res, target) => Debug.LogError($"result {res}, target apps : {target}"))
		//     .Share();
		NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* L_6 = (NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B*)il2cpp_codegen_object_new(NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		NativeShare__ctor_m7808C679AB06B9BE0BFF3FA5FF0F7FD06C51BE6E(L_6, NULL);
		String_t* L_7 = __this->___U3CpathU3E5__1_3;
		NullCheck(L_6);
		NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* L_8;
		L_8 = NativeShare_AddFile_m2B6FF1B19C51E3229002A8430D253C1EFC0DD240(L_6, L_7, (String_t*)NULL, NULL);
		NullCheck(L_8);
		NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* L_9;
		L_9 = NativeShare_SetSubject_mB0E01F5228D5C868274067653497BFD0F1EF1D8A(L_8, _stringLiteralAA473D3C6DEC47486536A8DCA1C955417EAEDF41, NULL);
		NullCheck(L_9);
		NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* L_10;
		L_10 = NativeShare_SetText_mEB89C82F62181F7754C20F3516754B81559DBA13(L_9, _stringLiteral7055A3A705CAD150F81C21ABD4C8B99578A13480, NULL);
		NullCheck(L_10);
		NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* L_11;
		L_11 = NativeShare_SetUrl_m5A85D24E53BA393226DD13089A85DC9044B294A6(L_10, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_il2cpp_TypeInfo_var);
		ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5* L_12 = ((U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_il2cpp_TypeInfo_var))->___U3CU3E9__9_0_1;
		ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5* L_13 = L_12;
		G_B9_0 = L_13;
		G_B9_1 = L_11;
		if (L_13)
		{
			G_B10_0 = L_13;
			G_B10_1 = L_11;
			goto IL_009e;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_il2cpp_TypeInfo_var);
		U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0* L_14 = ((U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_il2cpp_TypeInfo_var))->___U3CU3E9_0;
		ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5* L_15 = (ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5*)il2cpp_codegen_object_new(ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5_il2cpp_TypeInfo_var);
		NullCheck(L_15);
		ShareResultCallback__ctor_m916C36D840FC04394DB3549444BFA1E4C79F8AD4(L_15, L_14, (intptr_t)((void*)U3CU3Ec_U3CTakeScreenShotAndShareU3Eb__9_0_m8F0A62F22FB592BCF910DF68CC8B45C402A7F5DC_RuntimeMethod_var), NULL);
		ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5* L_16 = L_15;
		((U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_il2cpp_TypeInfo_var))->___U3CU3E9__9_0_1 = L_16;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t891034FBD1CE4397BD58BA454E5BFDF34D7207B0_il2cpp_TypeInfo_var))->___U3CU3E9__9_0_1), (void*)L_16);
		G_B10_0 = L_16;
		G_B10_1 = G_B9_1;
	}

IL_009e:
	{
		NullCheck(G_B10_1);
		NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B* L_17;
		L_17 = NativeShare_SetCallback_m546C0EEF61664E6B0B4A0318044A16BBBA0B5179(G_B10_1, G_B10_0, NULL);
		NullCheck(L_17);
		NativeShare_Share_m3DB5879D819C16FC4325D124E296F6C6CD9C13DD(L_17, NULL);
		// }
		return (bool)0;
	}
}
// System.Object Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTakeScreenShotAndShareU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE8F34F055B451C78BCD024B7D511F91AE35052AE (U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTakeScreenShotAndShareU3Ed__9_System_Collections_IEnumerator_Reset_mDAD2C4081C586643306F12C63330087F76F534B7 (U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTakeScreenShotAndShareU3Ed__9_System_Collections_IEnumerator_Reset_mDAD2C4081C586643306F12C63330087F76F534B7_RuntimeMethod_var)));
	}
}
// System.Object Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTakeScreenShotAndShareU3Ed__9_System_Collections_IEnumerator_get_Current_m1E98DE008F762FDF12D633DC1A042F19C525609C (U3CTakeScreenShotAndShareU3Ed__9_t37D2AC6A12C06C0815769DA307017B40812D3045* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
