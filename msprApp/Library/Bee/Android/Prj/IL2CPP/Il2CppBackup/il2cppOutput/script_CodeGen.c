﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void InterfaceAndShareCreation::onClickShare()
extern void InterfaceAndShareCreation_onClickShare_mE7D824C00FAE07677885A55E70B0959DF9659E7D (void);
// 0x00000002 System.Collections.IEnumerator InterfaceAndShareCreation::takeScreenShotAndShare()
extern void InterfaceAndShareCreation_takeScreenShotAndShare_m06DE12C46F0CE2D4E2AE5FFE5A03EE7DDA4D9B91 (void);
// 0x00000003 System.Void InterfaceAndShareCreation::.ctor()
extern void InterfaceAndShareCreation__ctor_m12318221668FA3A5A9EBA56A5E8D993C90392114 (void);
// 0x00000004 System.Void InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::.ctor(System.Int32)
extern void U3CtakeScreenShotAndShareU3Ed__4__ctor_m9D9AA0A1FB3C7E0B3AB1C1371024C672B44C7F3C (void);
// 0x00000005 System.Void InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::System.IDisposable.Dispose()
extern void U3CtakeScreenShotAndShareU3Ed__4_System_IDisposable_Dispose_m27440EA5327B75ED8170A14A34B0B0CF166BE91E (void);
// 0x00000006 System.Boolean InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::MoveNext()
extern void U3CtakeScreenShotAndShareU3Ed__4_MoveNext_mEDF9CB39447555EE99D8B1B1815D45C0B0317363 (void);
// 0x00000007 System.Object InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtakeScreenShotAndShareU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25B4A0ADEB37E2653490C9237962CB3BF8149C99 (void);
// 0x00000008 System.Void InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::System.Collections.IEnumerator.Reset()
extern void U3CtakeScreenShotAndShareU3Ed__4_System_Collections_IEnumerator_Reset_m1100C1B8AFCD31C33805C692A5EA3829B93ED312 (void);
// 0x00000009 System.Object InterfaceAndShareCreation/<takeScreenShotAndShare>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CtakeScreenShotAndShareU3Ed__4_System_Collections_IEnumerator_get_Current_mFB72C197B1482C31171FF54C68FC859FC970DCAA (void);
// 0x0000000A System.Void ScriptOfRotation::Start()
extern void ScriptOfRotation_Start_m69D4C4328177E6762A32E8A7B9C0CE545E888827 (void);
// 0x0000000B System.Void ScriptOfRotation::Update()
extern void ScriptOfRotation_Update_m81CEFC54FC3B8318B28D4EBB20BF43B43D27F750 (void);
// 0x0000000C System.Void ScriptOfRotation::.ctor()
extern void ScriptOfRotation__ctor_mCCE0AF7D2080009AC3FDA14ADDD9FABC17070D90 (void);
// 0x0000000D System.Void ShareSocialMedia::.ctor()
extern void ShareSocialMedia__ctor_m33388428688706DC50F42F1AB64B69A74DD273C0 (void);
// 0x0000000E Assets.Scripts.CRMRequest Assets.Scripts.CRMLink::RetrieveData(System.String,System.String)
extern void CRMLink_RetrieveData_m73A09E347AFFC05FD0775C95997954A9F7010A40 (void);
// 0x0000000F System.Void Assets.Scripts.CRMLink::.ctor()
extern void CRMLink__ctor_m8D6FA061FD706D49EB100120D2B6DC8581C1EB12 (void);
// 0x00000010 System.Void Assets.Scripts.CRMRequest::.ctor(System.String,System.String,System.String)
extern void CRMRequest__ctor_mAFE2111C4BC7CBCAFAE5F5287D3184C2D97A5511 (void);
// 0x00000011 System.Collections.IEnumerator Assets.Scripts.CRMRequest::ExecuteRequest()
extern void CRMRequest_ExecuteRequest_mB68851B83798BA37E6FB4E681CC53EAFDA25C243 (void);
// 0x00000012 T Assets.Scripts.CRMRequest::getResult()
// 0x00000013 System.Void Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::.ctor(System.Int32)
extern void U3CExecuteRequestU3Ed__5__ctor_m1FC9C91CFF1F90B800ABA80927A69673776D85D9 (void);
// 0x00000014 System.Void Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::System.IDisposable.Dispose()
extern void U3CExecuteRequestU3Ed__5_System_IDisposable_Dispose_mC2D7D9CB43A2119FF53D1D201714AED5DE6343EA (void);
// 0x00000015 System.Boolean Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::MoveNext()
extern void U3CExecuteRequestU3Ed__5_MoveNext_m6D86FA1D5EA48ACEEE511E27EC7F9A7125A7B283 (void);
// 0x00000016 System.Void Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::<>m__Finally1()
extern void U3CExecuteRequestU3Ed__5_U3CU3Em__Finally1_mB9CCA43D3A8189903DBB1523E5D6960DAD73CA06 (void);
// 0x00000017 System.Object Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExecuteRequestU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m399C5FABAD85503F0A3C2E0E93C7E8BFF71100D8 (void);
// 0x00000018 System.Void Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::System.Collections.IEnumerator.Reset()
extern void U3CExecuteRequestU3Ed__5_System_Collections_IEnumerator_Reset_m7B0FA514DC8A14D22DF202136F36FD06D715FFDD (void);
// 0x00000019 System.Object Assets.Scripts.CRMRequest/<ExecuteRequest>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CExecuteRequestU3Ed__5_System_Collections_IEnumerator_get_Current_m789B018E9831759ECBEC342A042C76C1D5D16779 (void);
// 0x0000001A System.Void Assets.Scripts.PopupShare::onClickSubmit()
extern void PopupShare_onClickSubmit_m47FDAB4CFEA0F40602145D23749EB9B1DA63FDF8 (void);
// 0x0000001B System.Void Assets.Scripts.PopupShare::onClickCancel()
extern void PopupShare_onClickCancel_m2BBEEB7D0FE62AEC47901F007FF2DE9701E998A9 (void);
// 0x0000001C System.Collections.IEnumerator Assets.Scripts.PopupShare::SendProspect()
extern void PopupShare_SendProspect_m003EBA708009483799FF2A3A59102FB9CC2C9355 (void);
// 0x0000001D System.String Assets.Scripts.PopupShare::RetrieveDataName()
extern void PopupShare_RetrieveDataName_m868A033D28CA3F41309605DD6AA9D45AC91BE295 (void);
// 0x0000001E System.String Assets.Scripts.PopupShare::RetrieveDataEmail()
extern void PopupShare_RetrieveDataEmail_mB102881EBEB87E60C26DED09BEBD9901F9802202 (void);
// 0x0000001F System.Void Assets.Scripts.PopupShare::ShareScore()
extern void PopupShare_ShareScore_m3671335DF1600A888B819F5A3B12D66445F184FC (void);
// 0x00000020 System.Collections.IEnumerator Assets.Scripts.PopupShare::TakeScreenShotAndShare()
extern void PopupShare_TakeScreenShotAndShare_mCF21B51D0B99F6BF90664078315A12FE483CBC24 (void);
// 0x00000021 System.Void Assets.Scripts.PopupShare::.ctor()
extern void PopupShare__ctor_mBA5BCD2941AD1642A515FBC849745BFCFCA29409 (void);
// 0x00000022 System.Void Assets.Scripts.PopupShare/<SendProspect>d__5::.ctor(System.Int32)
extern void U3CSendProspectU3Ed__5__ctor_m8C002DE64B385DD045C0A29C316B07D6627D1750 (void);
// 0x00000023 System.Void Assets.Scripts.PopupShare/<SendProspect>d__5::System.IDisposable.Dispose()
extern void U3CSendProspectU3Ed__5_System_IDisposable_Dispose_mF560542FDB35C682B86C1BC34DFF98D422DDF311 (void);
// 0x00000024 System.Boolean Assets.Scripts.PopupShare/<SendProspect>d__5::MoveNext()
extern void U3CSendProspectU3Ed__5_MoveNext_m00BE9FB63626380C7DD6DBCB21D12230825518E5 (void);
// 0x00000025 System.Object Assets.Scripts.PopupShare/<SendProspect>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendProspectU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2E4E75AEBFEAEC93BB7612BF2FC67DE3953929A (void);
// 0x00000026 System.Void Assets.Scripts.PopupShare/<SendProspect>d__5::System.Collections.IEnumerator.Reset()
extern void U3CSendProspectU3Ed__5_System_Collections_IEnumerator_Reset_m7AC1900C505A46F9E0BF9AA3868A136401C50D97 (void);
// 0x00000027 System.Object Assets.Scripts.PopupShare/<SendProspect>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CSendProspectU3Ed__5_System_Collections_IEnumerator_get_Current_m918BE55145B758EA758CEF4F871FDD670602A601 (void);
// 0x00000028 System.Void Assets.Scripts.PopupShare/<>c::.cctor()
extern void U3CU3Ec__cctor_m41313CCDEEE0F8FBF9A84FAA9981F81AE03A2B4F (void);
// 0x00000029 System.Void Assets.Scripts.PopupShare/<>c::.ctor()
extern void U3CU3Ec__ctor_m0543D9E3F5983200FBDAEDA716EBDEA881C496DC (void);
// 0x0000002A System.Void Assets.Scripts.PopupShare/<>c::<TakeScreenShotAndShare>b__9_0(NativeShare/ShareResult,System.String)
extern void U3CU3Ec_U3CTakeScreenShotAndShareU3Eb__9_0_m8F0A62F22FB592BCF910DF68CC8B45C402A7F5DC (void);
// 0x0000002B System.Void Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::.ctor(System.Int32)
extern void U3CTakeScreenShotAndShareU3Ed__9__ctor_m070ADBD890C72EFFED92984E3B99570B2EDDCEBB (void);
// 0x0000002C System.Void Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::System.IDisposable.Dispose()
extern void U3CTakeScreenShotAndShareU3Ed__9_System_IDisposable_Dispose_mC9BA77A0F67E86FAB3B95737C42D1DC4EA122945 (void);
// 0x0000002D System.Boolean Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::MoveNext()
extern void U3CTakeScreenShotAndShareU3Ed__9_MoveNext_mB2C48578AC3DF1F0E0D6A6BCB9663DF788694155 (void);
// 0x0000002E System.Object Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTakeScreenShotAndShareU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE8F34F055B451C78BCD024B7D511F91AE35052AE (void);
// 0x0000002F System.Void Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::System.Collections.IEnumerator.Reset()
extern void U3CTakeScreenShotAndShareU3Ed__9_System_Collections_IEnumerator_Reset_mDAD2C4081C586643306F12C63330087F76F534B7 (void);
// 0x00000030 System.Object Assets.Scripts.PopupShare/<TakeScreenShotAndShare>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CTakeScreenShotAndShareU3Ed__9_System_Collections_IEnumerator_get_Current_m1E98DE008F762FDF12D633DC1A042F19C525609C (void);
static Il2CppMethodPointer s_methodPointers[48] = 
{
	InterfaceAndShareCreation_onClickShare_mE7D824C00FAE07677885A55E70B0959DF9659E7D,
	InterfaceAndShareCreation_takeScreenShotAndShare_m06DE12C46F0CE2D4E2AE5FFE5A03EE7DDA4D9B91,
	InterfaceAndShareCreation__ctor_m12318221668FA3A5A9EBA56A5E8D993C90392114,
	U3CtakeScreenShotAndShareU3Ed__4__ctor_m9D9AA0A1FB3C7E0B3AB1C1371024C672B44C7F3C,
	U3CtakeScreenShotAndShareU3Ed__4_System_IDisposable_Dispose_m27440EA5327B75ED8170A14A34B0B0CF166BE91E,
	U3CtakeScreenShotAndShareU3Ed__4_MoveNext_mEDF9CB39447555EE99D8B1B1815D45C0B0317363,
	U3CtakeScreenShotAndShareU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25B4A0ADEB37E2653490C9237962CB3BF8149C99,
	U3CtakeScreenShotAndShareU3Ed__4_System_Collections_IEnumerator_Reset_m1100C1B8AFCD31C33805C692A5EA3829B93ED312,
	U3CtakeScreenShotAndShareU3Ed__4_System_Collections_IEnumerator_get_Current_mFB72C197B1482C31171FF54C68FC859FC970DCAA,
	ScriptOfRotation_Start_m69D4C4328177E6762A32E8A7B9C0CE545E888827,
	ScriptOfRotation_Update_m81CEFC54FC3B8318B28D4EBB20BF43B43D27F750,
	ScriptOfRotation__ctor_mCCE0AF7D2080009AC3FDA14ADDD9FABC17070D90,
	ShareSocialMedia__ctor_m33388428688706DC50F42F1AB64B69A74DD273C0,
	CRMLink_RetrieveData_m73A09E347AFFC05FD0775C95997954A9F7010A40,
	CRMLink__ctor_m8D6FA061FD706D49EB100120D2B6DC8581C1EB12,
	CRMRequest__ctor_mAFE2111C4BC7CBCAFAE5F5287D3184C2D97A5511,
	CRMRequest_ExecuteRequest_mB68851B83798BA37E6FB4E681CC53EAFDA25C243,
	NULL,
	U3CExecuteRequestU3Ed__5__ctor_m1FC9C91CFF1F90B800ABA80927A69673776D85D9,
	U3CExecuteRequestU3Ed__5_System_IDisposable_Dispose_mC2D7D9CB43A2119FF53D1D201714AED5DE6343EA,
	U3CExecuteRequestU3Ed__5_MoveNext_m6D86FA1D5EA48ACEEE511E27EC7F9A7125A7B283,
	U3CExecuteRequestU3Ed__5_U3CU3Em__Finally1_mB9CCA43D3A8189903DBB1523E5D6960DAD73CA06,
	U3CExecuteRequestU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m399C5FABAD85503F0A3C2E0E93C7E8BFF71100D8,
	U3CExecuteRequestU3Ed__5_System_Collections_IEnumerator_Reset_m7B0FA514DC8A14D22DF202136F36FD06D715FFDD,
	U3CExecuteRequestU3Ed__5_System_Collections_IEnumerator_get_Current_m789B018E9831759ECBEC342A042C76C1D5D16779,
	PopupShare_onClickSubmit_m47FDAB4CFEA0F40602145D23749EB9B1DA63FDF8,
	PopupShare_onClickCancel_m2BBEEB7D0FE62AEC47901F007FF2DE9701E998A9,
	PopupShare_SendProspect_m003EBA708009483799FF2A3A59102FB9CC2C9355,
	PopupShare_RetrieveDataName_m868A033D28CA3F41309605DD6AA9D45AC91BE295,
	PopupShare_RetrieveDataEmail_mB102881EBEB87E60C26DED09BEBD9901F9802202,
	PopupShare_ShareScore_m3671335DF1600A888B819F5A3B12D66445F184FC,
	PopupShare_TakeScreenShotAndShare_mCF21B51D0B99F6BF90664078315A12FE483CBC24,
	PopupShare__ctor_mBA5BCD2941AD1642A515FBC849745BFCFCA29409,
	U3CSendProspectU3Ed__5__ctor_m8C002DE64B385DD045C0A29C316B07D6627D1750,
	U3CSendProspectU3Ed__5_System_IDisposable_Dispose_mF560542FDB35C682B86C1BC34DFF98D422DDF311,
	U3CSendProspectU3Ed__5_MoveNext_m00BE9FB63626380C7DD6DBCB21D12230825518E5,
	U3CSendProspectU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2E4E75AEBFEAEC93BB7612BF2FC67DE3953929A,
	U3CSendProspectU3Ed__5_System_Collections_IEnumerator_Reset_m7AC1900C505A46F9E0BF9AA3868A136401C50D97,
	U3CSendProspectU3Ed__5_System_Collections_IEnumerator_get_Current_m918BE55145B758EA758CEF4F871FDD670602A601,
	U3CU3Ec__cctor_m41313CCDEEE0F8FBF9A84FAA9981F81AE03A2B4F,
	U3CU3Ec__ctor_m0543D9E3F5983200FBDAEDA716EBDEA881C496DC,
	U3CU3Ec_U3CTakeScreenShotAndShareU3Eb__9_0_m8F0A62F22FB592BCF910DF68CC8B45C402A7F5DC,
	U3CTakeScreenShotAndShareU3Ed__9__ctor_m070ADBD890C72EFFED92984E3B99570B2EDDCEBB,
	U3CTakeScreenShotAndShareU3Ed__9_System_IDisposable_Dispose_mC9BA77A0F67E86FAB3B95737C42D1DC4EA122945,
	U3CTakeScreenShotAndShareU3Ed__9_MoveNext_mB2C48578AC3DF1F0E0D6A6BCB9663DF788694155,
	U3CTakeScreenShotAndShareU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE8F34F055B451C78BCD024B7D511F91AE35052AE,
	U3CTakeScreenShotAndShareU3Ed__9_System_Collections_IEnumerator_Reset_mDAD2C4081C586643306F12C63330087F76F534B7,
	U3CTakeScreenShotAndShareU3Ed__9_System_Collections_IEnumerator_get_Current_m1E98DE008F762FDF12D633DC1A042F19C525609C,
};
static const int32_t s_InvokerIndices[48] = 
{
	7397,
	7264,
	7397,
	5765,
	7397,
	7161,
	7264,
	7397,
	7264,
	7397,
	7397,
	7397,
	7397,
	10021,
	7397,
	1664,
	7264,
	0,
	5765,
	7397,
	7161,
	7397,
	7264,
	7397,
	7264,
	7397,
	7397,
	7264,
	7264,
	7264,
	7397,
	7264,
	7397,
	5765,
	7397,
	7161,
	7264,
	7397,
	7264,
	11520,
	7397,
	2865,
	5765,
	7397,
	7161,
	7264,
	7397,
	7264,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000012, { 0, 1 } },
};
extern const uint32_t g_rgctx_JsonConvert_DeserializeObject_TisT_t32172930BAD9834C583C81868181D9C16D186622_m75550F1C229AB10E1EBABFAE2C3B7ED9B58A47A7;
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConvert_DeserializeObject_TisT_t32172930BAD9834C583C81868181D9C16D186622_m75550F1C229AB10E1EBABFAE2C3B7ED9B58A47A7 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_script_CodeGenModule;
const Il2CppCodeGenModule g_script_CodeGenModule = 
{
	"script.dll",
	48,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
