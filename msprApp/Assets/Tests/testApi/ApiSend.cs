
using Assets.Scripts;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

public class ApiSend
{

    [Test]
    public void ApiDataTest()
    {
        string emailToSend = "testauto@gmail.com";
        string nameToSend = "LeTestAutomatique";
        var test = CRMLink.RetrieveData(emailToSend,nameToSend);

        string data = "{\"fields\":[{\"objectTypeId\": \"0-1\",\"name\":\"email\",\"value\": \" " + emailToSend + " \" },{\"objectTypeId\": \"0-1\",\"name\":\"firstname\",\"value\": \" " + nameToSend + " \"  }]}";

        var res = data.Equals(test.data);
        Assert.True(res);
        

    }


}
