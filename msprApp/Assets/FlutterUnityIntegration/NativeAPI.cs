﻿using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;
using System;

namespace FlutterUnityIntegration
{
    public class NativeAPI
    {
#if UNITY_IOS && !UNITY_EDITOR
    [DllImport("__Internal")]
    public static extern void OnUnityMessage(string message);

    [DllImport("__Internal")]
    public static extern void OnUnitySceneLoaded(string name, int buildIndex, bool isLoaded, bool IsValid);
#endif

#if UNITY_WEBGL
        [DllImport("__Internal")]
        public static extern void OnUnityMessageWeb(string message);

        [DllImport("__Internal")]
        public static extern void OnUnitySceneLoadedWeb(string name, int buildIndex, bool isLoaded, bool isValid);
#endif

        public static void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
#if UNITY_WEBGL
            OnUnitySceneLoadedWeb(scene.name, scene.buildIndex, scene.isLoaded, scene.IsValid());
#elif UNITY_IOS && !UNITY_EDITOR
        OnUnitySceneLoaded(scene.name, scene.buildIndex, scene.isLoaded, scene.IsValid());
#endif
        }

        public static void SendMessageToFlutter(string message)
        {

#if UNITY_WEBGL
        OnUnityMessageWeb(message);
#elif UNITY_IOS && !UNITY_EDITOR
        OnUnityMessage(message);
#endif
        }

        public static void ShowHostMainWindow()
        {

#if UNITY_IOS && !UNITY_EDITOR
        // NativeAPI.showHostMainWindow();
#endif
        }

        public static void UnloadMainWindow()
        {

#if UNITY_IOS && !UNITY_EDITOR
        // NativeAPI.unloadPlayer();
#endif
        }

        public static void QuitUnityWindow()
        {

#if UNITY_IOS && !UNITY_EDITOR
        // NativeAPI.quitPlayer();
#endif
        }
    }
}