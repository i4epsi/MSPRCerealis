﻿using Newtonsoft.Json;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;

namespace Assets.Scripts
{

    public class CRMRequest
    {
        string url;
        public string data;
        string method;
        public string result;

        public CRMRequest(string url, string data, string method)
        {
            this.url = url;
            this.data = data;
            this.method = method;
        }

        public IEnumerator ExecuteRequest()
        {
            using UnityWebRequest request = new UnityWebRequest(this.url, this.method);
            request.SetRequestHeader("Content-Type", "application/json");
		request.SetRequestHeader("Authorization", "Bearer pat-eu1-a18a3a99-b22b-410d-838b-b58f08bbcba0");
		byte[] bodyRaw = Encoding.UTF8.GetBytes(this.data);
            request.uploadHandler = new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = new DownloadHandlerBuffer();
		

            yield return request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.Success)
            {
                Debug.LogError(request.result);
                this.result = request.downloadHandler.text;
                Debug.LogError(request.downloadHandler.text);
            } 
            else
            {
                Debug.LogError(request.result);
                Debug.LogError(request.responseCode);
                Debug.LogError(request.downloadHandler.text);
            }
        }

        public T getResult<T>()
        {
            if (this.result != null)
            {
                return JsonConvert.DeserializeObject<T>(this.result);
            }
            return default(T);
        }
    }
}