using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceAndShareCreation : MonoBehaviour
{
    public string photoName = "temp_screenshot";
    public string popupName = "PopupSharing";

    public GameObject popupCanvasPrefab = null;

    public void onClickShare()
    {
        if (!GameObject.Find(popupName))
        {
            StartCoroutine(takeScreenShotAndShare());
        }
    }

    IEnumerator takeScreenShotAndShare()
    {
        yield return new WaitForEndOfFrame();

        Texture2D tx = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        tx.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        tx.Apply();

        string path = Path.Combine(Application.temporaryCachePath, "sharedImage.png");//image name
        File.WriteAllBytes(path, tx.EncodeToPNG());

        Destroy(tx); //to avoid memory leaks

        GameObject popupCanvasInstance = Instantiate(popupCanvasPrefab);
        popupCanvasInstance.name = "PopupSharing";
    }
}
