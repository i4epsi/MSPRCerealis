﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts
{
    public class CRMLink
    {
        public static CRMRequest RetrieveData(string emailToSend, string nameToSend)
        {
		Debug.Log("email:" + emailToSend);
		Debug.Log("name:" + nameToSend);

            string url = "https://api.hsforms.com/submissions/v3/integration/submit/25923445/4e078095-3f17-4a6a-b26f-938676b23215";
            string data = "{\"fields\":[{\"objectTypeId\": \"0-1\",\"name\":\"email\",\"value\": \" "+emailToSend+" \" },{\"objectTypeId\": \"0-1\",\"name\":\"firstname\",\"value\": \" "+nameToSend+" \"  }]}";
            Debug.Log(data);
            return new CRMRequest(url, data, "POST");
        }

    }
}