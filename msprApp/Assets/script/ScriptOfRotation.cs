using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptOfRotation : MonoBehaviour
{
    public float rotateSpeed = 20;

    void Start() { }

    void Update()
    {
        transform.Rotate(0, rotateSpeed * Time.deltaTime, 0);
    }
}
