using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Newtonsoft.Json;
using UnityEngine.Networking;
using System.IO;

namespace Assets.Scripts
{
    public class PopupShare : MonoBehaviour
    {
        public string photoName = "temp_screenshot";
        public string hashTags = "#cerealis #coloring #AR";
        public string chooserText = "Share with ...";

        public void onClickSubmit() {
            StartCoroutine(SendProspect());
            
        }

        public void onClickCancel()
        {
            Destroy(gameObject);
        }

        public IEnumerator SendProspect()
        {
            string email = RetrieveDataEmail();
            string name = RetrieveDataName();

            var request = CRMLink.RetrieveData(email, name);
            yield return request.ExecuteRequest();
            ShareScore();

        }
    	  public string RetrieveDataName()
        {
	      GameObject t = GameObject.Find("nameText");
            Text nameTextComponent = t.GetComponent<Text>();
            string name = nameTextComponent.text;
            return name;
        }
    	  public string RetrieveDataEmail()
        {
	      GameObject t = GameObject.Find("emailText");
            Text nameTextComponent = t.GetComponent<Text>();
            string email = nameTextComponent.text;
            return email;
        }
        public void ShareScore()
        {
            StartCoroutine(TakeScreenShotAndShare());
        }

        IEnumerator TakeScreenShotAndShare()
        {

            yield return new WaitForEndOfFrame();


            string path = Path.Combine(Application.temporaryCachePath, "sharedImage.png");
            new NativeShare()
                .AddFile(path)
                .SetSubject("I finish my draw !!!!")
                .SetText("#cerealis #coloring #AR").SetUrl("").SetCallback((res, target) => Debug.LogError($"result {res}, target apps : {target}"))
                .Share();

        }

    }
}